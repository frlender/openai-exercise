import numpy as np
from scipy.spatial.distance import cdist
import pickle
from sklearn.preprocessing import StandardScaler

class Evo():
    def __init__(self,episodes='data.pkl',top=5,n=5):
        self.n = n

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        episodes_stats = [[x]+list(self.getMaxReturn(x)) for x in episodes]
        top_episodes_stats = sorted(
            episodes_stats,
            key = lambda x:x[2],
            reverse = True
        )[:top]

        for i, episode_stats in enumerate(top_episodes_stats):
            if i == 0:
                episode = episode_stats[0]
                episode = episode[:episode_stats[1]+1]
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                episode = episode_stats[0]
                episode = episode[:episode_stats[1]+1]
                self.add(episode)

        self.scaler = StandardScaler()
        self.observations = self.scaler.fit_transform(self.observations)

    def getMaxReturn(self,episode):
        cumsum = np.cumsum([x[2] for x in episode])
        idx = np.argmax(cumsum)
        return idx, cumsum[idx]

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs):
        obs = self.scaler.transform([obs])
        mat = cdist(obs, self.observations, 'euclidean')
        vector = np.squeeze(mat)
        actionMat = self.actions[np.argsort(vector),:]
        actionMat = actionMat[:self.n,:]
        means = np.mean(actionMat,axis=0)
        stds = np.std(actionMat,axis=0)
        action = np.random.normal(means,stds)
        return action

        # obs = self.scaler.transform([obs])
        # mat = cdist(obs, self.observations, 'euclidean')
        # vector = np.squeeze(mat)
        # actionVec = self.actions[np.argsort(vector)]
        # actionVec = actionVec[:self.n]
        # action = np.random.normal(np.mean(actionVec),np.std(actionVec))
        # return action
