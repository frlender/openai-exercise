import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle


tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#evo = Evo(qn.loadPkl('best/data_e.pkl'),rf_n=20)
#qn.dumpPkl(evo,'best/best_evo_e.pkl')

#%%

evo = qn.loadPkl('best/best_evo_e.pkl')

episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs,0.6)[0],
        max_iter=5000,
        render=True
)
ret = evo.getMaxReturn(episode)[1]
tracker.isBest(ret)
print("{} timesteps, current {:.3f}."\
.format(t, ret))


#%%
import matplotlib.pyplot as plt

obs = [x[0] for x in episode]
obs = np.vstack(obs)
x = range(obs.shape[0])
for i in [4,9]:
    plt.plot(x,obs[:,i],label=str(i))
plt.legend()

percent = 0.1
thres = 1/4
dimLen = obs.shape[0]
lowerIdx = int(dimLen*percent)
upperIdx = int(dimLen*(1-percent))+1
middleObs = obs[lowerIdx:upperIdx,:]

hipsAngle = middleObs[:,[4,9]]
count = hipsAngle.shape[0]
cc = (np.sum(hipsAngle[:,0] < hipsAngle[:,1])/count > thres and
      np.sum(hipsAngle[:,0] > hipsAngle[:,1])/count > thres)

def getSymmetry(episode):
    actions = [x[1] for x in episode]
    actions = np.vstack(actions)
    # maybe median is better?
    avgs = np.mean(actions,axis=0)
    return np.abs(avgs[0]-avgs[2]) + np.abs(avgs[1]-avgs[3])

qn.dumpPkl(episode, '../symmetry/data3.pkl')