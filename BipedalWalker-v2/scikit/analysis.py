import pickle
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
import numpy as np
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split, cross_val_score
from sklearn.svm import SVR

from EvoModel import Evo

evo = Evo(episodes='data3.pkl', top=0.034, rf_n=10)
obs = evo.observations
actions = evo.actions

x_train, x_test, y_train, y_test = train_test_split(obs,actions,test_size=0.2)


x_plot = []
y_plot = []
err = []

for param in range(75,101,5):
    print(param)
    model = Pipeline([
        ('rf', RandomForestRegressor(n_estimators=param))
    ])
    scores = cross_val_score(model, x_train, y_train)
    x_plot.append(param)
    y_plot.append(np.mean(scores))
    err.append(np.std(scores))

plt.errorbar(x_plot,y_plot,err)
plt.savefig('figures/rf_n_top_100.png')


model = SVR()
scores = cross_val_score(model, x_train, y_train)
meanScore = np.mean(scores)
stdScore = np.std(scores)