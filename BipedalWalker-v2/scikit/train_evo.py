import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import matplotlib.pyplot as plt

from EvoModel import Evo

# top = 60
# session_len = 300

top = 0.1
rf_n = 10
session_len = 100
epsilon = lambda x:np.abs(x-100)/300

# env = gym.make('MountainCarContinuous-v0')
#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)

#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)

tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')
evo = Evo(episodes='../data.pkl', top=top, rf_n=rf_n)

episodes = []
performance = []

for i in range(0,6000):
    episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,epsilon(tracker.best)),
            max_iter=5000
    )
    ret = evo.getMaxReturn(episode)[1]
    if tracker.isBest(ret):
        best_evo = evo
    print("{}th episode, {} timesteps, current {:.3f}, best {:.3f}."\
    .format(i, t, ret, tracker.best))
    episodes.append(episode)

    if (i+1)%10 == 0:
        performance.append([i+1, tracker.best])

    if (i+1)%session_len == 0:
        evo = Evo(episodes,top=top, rf_n=rf_n)
        episodes = []

    if (i+1)%1000 == 0:
        with open('models/{}_top_{}.pkl'.format(i+1, top),'wb') as mf:
            pickle.dump(evo,mf)
        with open('performance.pkl','wb') as pf:
            pickle.dump(performance,pf)

qn.dumpPkl(best_evo,'best_evo.pkl')