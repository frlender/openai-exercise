import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import matplotlib.pyplot as plt

#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)

#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)

tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')
#evo = Evo(top=top)
with open('../models/21000_top_120.pkl','rb') as mf:
    evo = pickle.load(mf)

episodes = []
for i in range(300):
    episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs),
            max_iter=5000
    )
    ret = evo.getMaxReturn(episode)[1]
    print("{}th episode, {} timesteps, current {:.3f}."\
    .format(i, t, ret))
    episodes.append(episode)

with open('data2.pkl','wb') as df:
    pickle.dump(episodes,df)

cc = list(map(lambda x:evo.getMaxReturn(x)[1],episodes))