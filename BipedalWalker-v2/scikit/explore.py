import pickle
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import StandardScaler


with open('../models/21000_top_120.pkl','rb') as mf:
    evo = pickle.load(mf)

observations = evo.observations
actions = evo.actions

x_train, x_test, y_train, y_test = train_test_split(observations,actions)

model = Pipeline([
        ('poly', PolynomialFeatures(degree=2)),
        ('linear', LinearRegression(fit_intercept=True))
        ])
model.fit(x_train,y_train)

score = model.score(x_test,y_test)

cb = observations[0,:]
cc = model.predict(cb)