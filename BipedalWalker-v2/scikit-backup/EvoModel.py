import numpy as np
from scipy.spatial.distance import cdist
import pickle
from sklearn.ensemble import RandomForestRegressor

class Evo():
    def __init__(self,episodes='data.pkl',top=10,rf_n=10):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        episodes_stats = [[x]+list(self.getMaxReturn(x)) for x in episodes]
        top_episodes_stats = sorted(
            episodes_stats,
            key = lambda x:x[2],
            reverse = True
        )[:top]

        for i, episode_stats in enumerate(top_episodes_stats):
            if i == 0:
                episode = episode_stats[0]
                episode = episode[:episode_stats[1]+1]
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                episode = episode_stats[0]
                episode = episode[:episode_stats[1]+1]
                self.add(episode)

        self.model = RandomForestRegressor(n_estimators=rf_n)
        self.model.fit(self.observations,self.actions)

        self.action_std = np.std(self.actions,axis=0)

    @staticmethod
    def getMaxReturn(episode):
        cumsum = np.cumsum([x[2] for x in episode])
        idx = np.argmax(cumsum)
        return idx, cumsum[idx]

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs, eps):
        action = self.model.predict([obs])
        if np.random.rand() < eps:
            action = np.random.normal(action, self.action_std)
#            action = env.action_space.sample()
        return action
