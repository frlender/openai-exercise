import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle

from EvoStandardized import Evo



tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
with open('models/11000_top_120.pkl','rb') as mf:
    evo = pickle.load(mf)

episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs),
        max_iter=5000,
        render=True
)
ret = evo.getMaxReturn(episode)[1]
tracker.isBest(ret)
print("{} timesteps, current {:.3f}."\
.format(t, ret))


#%%
x = []
for item in episodes:
    x.append(evo.getMaxReturn(item)[1])
x = sorted(x,reverse=True)