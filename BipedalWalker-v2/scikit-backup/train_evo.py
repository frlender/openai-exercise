import gym
from gym import wrappers
import qn
import numpy as np
import pa



from EvoModel import Evo
evo = None

top = 20
rf_n = 15
session_len = 200
eps = lambda x: np.abs(x-150)/300
max_iter = 2000


tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

episodes = []

for i in range(0,int(1e4)):
    if evo:
        episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs, eps(tracker.best)),
        max_iter=max_iter)
    else:
        episode, t = trainer.run_once(
        lambda obs:trainer.env.action_space.sample(),
        max_iter=max_iter)
    ret = Evo.getMaxReturn(episode)[1]
    tracker.isBest(ret)
    print("{}th episode, {} timesteps, current {:.3f}, best {:.3f}."\
    .format(i, t, ret, tracker.best))
    episodes.append(episode)


    if (i+1)%session_len == 0:
        break
        evo = Evo(episodes,top=top,rf_n=rf_n)
        episodes = []

    # if (i+1)%1000 == 0:
    #     with open('models/{}_top_{}.pkl'.format(i+1, top),'wb') as mf:
    #         pickle.dump(evo,mf)
    #     with open('performance.pkl','wb') as pf:
    #         pickle.dump(performance,pf)

qn.dumpPkl(episodes,'data_e.pkl')
qn.dumpPkl(evo,'best_evo.pkl')