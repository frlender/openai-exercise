import gym
import pickle


env = gym.make('BipedalWalker-v2')
episodes = []

for i_episode in range(int(300)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
#        env.render()
        action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps."\
            .format(i_episode, t+1))
            break
    episodes.append(episode)

with open('data.pkl','wb') as df:
    pickle.dump(episodes,df)

cc = []
for x in episodes:
    if len(x) > 1000:
        cc.append(x)

import numpy as np
def getMaxReturn(episode):
    cumsum = np.cumsum([x[2] for x in episode])
    idx = np.argmax(cumsum)
    return [idx,cumsum[idx]]
cc = []
for x in episodes:
    cc.append([len(x)]+list(getMaxReturn(x)))
cc = sorted(cc,key=lambda x:x[2],reverse=True)
    
ct = sorted(zip(episodes,cc),key=lambda x:x[1], reverse=True)