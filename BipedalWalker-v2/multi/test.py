import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle



trainer = pa.Trainer('BipedalWalker-v2')

#%%
with open('models/80_top_20_avg_14.26.pkl','rb') as mf:
    evos = pickle.load(mf)

i = 0

episode, t = trainer.run_once(
        lambda obs:evos[i].getAction(obs,0.1),
        max_iter=5000,
        render=True
)
ret = evos[i].getMaxReturn(episode)[1]
print("{} timesteps, current {:.3f}.".format(t, ret))


#%%
from EvoModel import Evo
print(Evo.isSymmetric(episode))

#%%
import matplotlib.pyplot as plt

obs = [x[0] for x in episode]
obs = np.vstack(obs)
x = range(obs.shape[0])
for i in [4,9]:
    plt.plot(x,obs[:,i],label=str(i))
plt.legend()