import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import matplotlib.pyplot as plt

from Trainer import Trainer
from EvoModel import Evo

agent_n = 20
session_len = 20
rf_n = 10

eps = lambda x:30/(50+x)

trainer = Trainer('BipedalWalker-v2')

# episodes = qn.loadPkl('../scikit/data3.pkl')
# top_episodes = Evo.getTopEpisodes(episodes, agent_n)
# agents = [Evo(episode, rf_n) for episode in top_episodes]
evos = ['random' for i in range(agent_n)]
# evos = qn.loadPkl('models/25_top_20_avg_87.78.pkl')

for i in range(0,40):
    episodes, ts = trainer.parallel(
        evos,
        eps(i),
        session_len
    )

    qn.dumpPkl(episodes, 'episodes.pkl')
    sorted_episodes = Evo.sortEpisodes(episodes)
    filtered = len(episodes) - len(sorted_episodes)
    count = np.min([agent_n, len(sorted_episodes)])
    top_episodes = sorted_episodes[:count]
    rets = [Evo.getMaxReturn(x)[1] for x in top_episodes]
    avgRet = np.mean(rets)

    evos = [Evo(episode, rf_n) for episode in top_episodes]

    print('{}th session, {} filtered, avgRet {:.2f}, best ret {:.2f}'.format(
    i+1, filtered, avgRet, rets[0]))

    if (i+1)%5 == 0:
        qn.dumpPkl(evos, 'models/{}_top_{}_avg_{:.2f}.pkl'.format(i+1,
        agent_n, avgRet))
