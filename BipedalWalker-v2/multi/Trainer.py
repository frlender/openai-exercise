import pa
from multiprocessing import Pool
import qn

class Trainer(pa.Trainer):
    def parallel(self,actionObjs, eps, session_len, cores=6, max_iter=2000):
        res = []
        for i_agent, actionObj in enumerate(actionObjs):
            params = []
            for i in range(session_len):
                params.append((self.envName, actionObj, eps, max_iter))

            with Pool(cores) as p:
                for i, item in enumerate(
                    p.imap_unordered(self._run_once, params)
                ):
                    res.append(item)

            qn.printEvery(5, i_agent+1, '{}th agent done.'.format(i_agent+1))
        return list(zip(*res))
