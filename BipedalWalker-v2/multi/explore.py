#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Sun Apr  9 20:37:34 2017

@author: qn
"""
import qn
from EvoModel import Evo
import matplotlib.pyplot as plt

episodes = qn.loadPkl('episodes.pkl')
cc = Evo.transform(episodes)
cd = list(filter(lambda x: len(x)>1, cc))
ce = list(map(lambda x: Evo.getHipsAngle(x,0),cd))

hipLeft = []
hipRight = []
for step in cd[0]:
    hipLeft.append(step[1][1])
    hipRight.append(step[1][3])

plt.plot(range(len(cd[0])), hipLeft)
plt.plot(range(len(cd[0])), hipRight)
