import pickle
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
import numpy as np
import qn


class Evo():
    def __init__(self,episode, rf_n=10):

        obs, action = self.get(episode)

        self.model = RandomForestRegressor(n_estimators=rf_n)
        self.model.fit(obs,action)
        self.action_std = np.std(action, axis=0)

    @classmethod
    def getMaxReturn(cls, episode):
        cumsum = np.cumsum([x[2] for x in episode])
        idx = np.argmax(cumsum)
        return idx, cumsum[idx]

    @classmethod
    def transform(cls, episodes):
        transformed = []
        for x in episodes:
            idx, _ = cls.getMaxReturn(x)
            transformed.append(x[:(idx+1)])
        return transformed

    @classmethod
    def getSymmetry(cls, episode, percent=0.1):
        obs = [x[0] for x in episode]
        obs = np.vstack(obs)

        dimLen = obs.shape[0]
        lowerIdx = int(dimLen*percent)
        upperIdx = int(dimLen*(1-percent))+1
        middleObs = obs[lowerIdx:upperIdx,:]

        hipsAngle = middleObs[:,[4,9]]
        hipsAngleMean = np.mean(hipsAngle,axis=0)

        return np.abs(hipsAngleMean[0]-hipsAngleMean[1])

    @classmethod
    def getHipsAngle(cls, episode, percent=0.1):
        obs = [x[0] for x in episode]
        obs = np.vstack(obs)

        dimLen = obs.shape[0]
        lowerIdx = int(dimLen*percent)
        upperIdx = int(dimLen*(1-percent))
        middleObs = obs[lowerIdx:upperIdx,:]

        hipsAngle = middleObs[:,[4,9]]
        count = hipsAngle.shape[0]
        return hipsAngle, count

    @classmethod
    def isSymmetric(cls, episode, thres=1/3, percent=0.1):
        hipsAngle, count = cls.getHipsAngle(episode, percent)
        return (np.sum(hipsAngle[:,0] < hipsAngle[:,1])/count > thres and
        np.sum(hipsAngle[:,0] > hipsAngle[:,1])/count > thres)

    @classmethod
    def isCentral(cls, episode, thres=1/10, percent=0.1):
        hipsAngle, count = cls.getHipsAngle(episode, percent)
        leftHipNeg = np.sum(hipsAngle[:,0] < 0)/count
        rightHipNeg = np.sum(hipsAngle[:,1] < 0)/count
        return (
            (leftHipNeg > thres and leftHipNeg < (1-thres)) and
            (rightHipNeg > thres and rightHipNeg < (1-thres))
        )

    @classmethod
    def sortEpisodes(cls, episodes):
        episodes = cls.transform(episodes)
        print(len(episodes))
        episodes = list(filter(lambda x:cls.isCentral(x), episodes))
        print(len(episodes))
        return qn.rpSort(episodes,[
            lambda x: -cls.getMaxReturn(x)[1],
            # cls.getSymmetry
        ])


    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action


    def getAction(self,obs,eps):
        action = self.model.predict([obs])[0]
        if np.random.rand() < eps:
            action = np.random.normal(action, self.action_std)
        return action
