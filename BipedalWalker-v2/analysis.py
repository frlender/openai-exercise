import pickle
import matplotlib.pyplot as plt
import seaborn as sb
import pandas as pd
import numpy as np

with open('models/21000_top_120.pkl','rb') as mf:
    evo = pickle.load(mf)

actionMat = evo.actions
df = pd.DataFrame(actionMat)
sb.pairplot(df)


with open('models/3000.pkl','rb') as mf:
    evo = pickle.load(mf)

actionMat = evo.actions
df = pd.DataFrame(actionMat)
plt.figure()
sb.pairplot(df)


with open('data.pkl','rb') as pf:
    data = pickle.load(pf)
actions = []
for e in data:
    for step in e:
        actions.append(step[1])
actionMat = np.vstack(actions[:10000])
df = pd.DataFrame(actionMat)
plt.figure()
sb.pairplot(df)



[0,1],[3,1],[1,2]
cov01 = np.cov(actionMat[:,[0,1]].T)
cov31 = np.cov(actionMat[:,[3,1]].T)
cov12 = np.cov(actionMat[:,[1,2]].T)
cov23 = np.cov(actionMat[:,[2,3]].T)

def getPlot(off_diag):
    cov = [[0.2,off_diag],
           [off_diag,0.2]]
    data = []
    for i in range(1000):
        data.append(np.random.multivariate_normal([0,0],cov))
    plt.figure()
    x,y = zip(*data)
    plt.scatter(x,y)
    plt.title('multi_normal {}'.format(off_diag))

def getPlot1(std):
    data = []
    for i in range(1000):
        data.append(np.random.normal([0,0],[std,std]))
    plt.figure()
    x,y = zip(*data)
    plt.scatter(x,y)
    plt.title('normal {}'.format(std))

getPlot(0)
getPlot1(0.44721)


def getPlot2():
    data = []
    for i in range(1000):
        data.append(np.random.standard_exponential(2))
    plt.figure()
    x,y = zip(*data)
    plt.scatter(x,y)
    plt.title('normal {}'.format(std))

getPlot2()
