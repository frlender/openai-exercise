import pa
import qn
import matplotlib.pyplot as plt

from Evo import Evo
import match as m
import numpy as np

top = 20
rf_n = 15
session_len = 200
# session_count = 10
eps = lambda x:30/(100+x)
# retThres = 10

trainer = pa.Trainer('BipedalWalker-v2')
# evo = Evo('data.pkl',rf_n=rf_n)
evo = qn.loadPkl('models/350_39.65.pkl')

for i in range(0,500):
    episodes = []
    n = 0
    while(len(episodes)<session_len):
        qn.printEvery(100, n, '{}th, {} epis'.format(n,len(episodes)))
        episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,eps(i)),
        )
        if m.match(episode):
            episodes.append(episode)
        n += 1

    epis = list(map(m.transform,episodes))
    epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
    rets = [m.getMaxReturn(x)[1] for x in epis]
    retThres =np.mean(rets)
    print('{}th session, avgRet {:.2f}, best ret {:.2f}'.format(
    i+1, retThres, rets[0]))

    evo = Evo(episodes=epis[:top],rf_n=rf_n)

    if (i+1) % 10 == 0:
        qn.dumpPkl(evo,'models/{}_{:.2f}.pkl'.format(i+1,np.mean(rets)))
