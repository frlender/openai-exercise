import numpy as np
import matplotlib.pyplot as plt


def getMaxReturn(episode):
    cumsum = np.cumsum([x[2] for x in episode])
    idx = np.argmax(cumsum)
    return idx, cumsum[idx]

def transform(episode):
    idx, _ = getMaxReturn(episode)
    return episode[:(idx+1)]

def getHipsAngle(episode, lower=0.05, upper=1):
    obs = [x[0] for x in episode]
    obs = np.vstack(obs)

    dimLen = obs.shape[0]
    lowerIdx = int(dimLen*lower)
    upperIdx = int(dimLen*upper)
    middleObs = obs[lowerIdx:upperIdx,:]

    hipsAngle = middleObs[:,[4,9]]
    count = hipsAngle.shape[0]
    return hipsAngle, count

def countChange(vec):
    count = 0
    gt0 = vec[0] > 0
    for item in vec[1:]:
        curr = item > 0
        if curr != gt0:
            count += 1
            gt0 = curr
    return count

def isAlter(hipsAngle, thres=1/3):
    gtCount = np.sum(hipsAngle[:,0] > hipsAngle[:,1])
    ratio = gtCount/hipsAngle.shape[0]
    return (ratio > thres and (1-ratio) > thres)

def isDiff(hipsAngle, thres=0.5):
    diffCount = np.sum(hipsAngle[:,0]*hipsAngle[:,1] < 0)
    ratio = diffCount/hipsAngle.shape[0]
    # print(diffCount, ratio)
    return ratio > thres

def match(episode):
    episode = transform(episode)
    isLong = len(episode) >= 5
    # ret = getMaxReturn(episode)[1]
    hipsAngle, count = getHipsAngle(episode)

    return isLong
    # return ( isLong and  ret > minRet )
    # return ( isLong and isAlter(hipsAngle,1/3) and isDiff(hipsAngle,0.6) )

def plot(obs):
    x = range(obs.shape[0])
    for i in [0,1]:
        plt.plot(x,obs[:,i],label=str(i))
    plt.legend()
