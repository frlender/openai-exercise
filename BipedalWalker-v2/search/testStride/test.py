import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import match as m

import matplotlib.pyplot as plt

from Evo import Evo


tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
evo = qn.loadPkl('../models/0.2_std/150_113.22.pkl')

episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs,0),
        max_iter=5000,  
        render=True
)
ret = m.getMaxReturn(episode)[1]
tracker.isBest(ret)
print("{} timesteps, current {:.3f}."\
.format(t, ret))


#%%
obs, _ = m.getHipsAngle(episode)
plt.figure()
m.plot(obs[:500])