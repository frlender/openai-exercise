import pa
import qn
import matplotlib.pyplot as plt

from Evo import Evo
import match as m
import numpy as np

top = 30
rf_n = 25
session_len = 200
# session_count = 10
eps = lambda x:30/(50+x)

trainer = pa.Trainer('BipedalWalker-v2')
# evo = Evo('data.pkl',rf_n=rf_n)
evo = qn.loadPkl('models/180_128.04.pkl')
best = 20

for i in range(180,500):
    episodes = []
    n = 0
    while(len(episodes)<session_len):
        qn.printEvery(100, n+1,
        '{}th, {} epis'.format(n+1,len(episodes)))

        episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,eps(i)),
        )
        if m.match(episode):
            episodes.append(episode)
        n += 1

    epis = list(map(m.transform,episodes))
    epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
    rets = [m.getMaxReturn(x)[1] for x in epis]
    retThres =np.mean(rets)
    print('{}th session, avgRet {:.2f}, best ret {:.2f}'.format(
    i+1, retThres, rets[0]))

    if retThres > best:
        best = retThres
        qn.dumpPkl(evo,'models/{}_{}_{:.2f}.pkl'.format(i+1,'30_25_0.25',
        np.mean(rets)))

    evo = Evo(episodes=epis[:top],rf_n=rf_n)
