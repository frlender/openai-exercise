import qn
import numpy as np
import pa
import matplotlib.pyplot as plt
import sys
sys.path.append('..')
import match as m
from Evo import Evo

from network import EvoNeuro
from EvoRBF import EvoRBF

tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
#evo = qn.loadPkl('../models/0.2_std/150_113.22.pkl')
#
#episodes = []
#for i in range(400):
#    episode, t = trainer.run_once(
#        lambda obs:evo.getAction(obs,0),
#        max_iter=5000,
#        render=False
#    )
#    episodes.append(episode)
#
#epis = list(map(m.transform,episodes))
#epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
#rets = [m.getMaxReturn(x)[1] for x in epis]
#qn.dumpPkl(epis[:30],'data.pkl')

#%%
def getRets(evo):
    episodes = []
    for i in range(20):
        qn.printEvery(10,i)
        episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,0),
            render=False
        )
        episodes.append(episode)
    epis2 = list(map(m.transform,episodes))
    epis2 = sorted(epis2, key=lambda x:-m.getMaxReturn(x)[1])
    rets = [m.getMaxReturn(x)[1] for x in epis2]
    return rets

topEpis = qn.loadPkl('data.pkl')
#evo = Evo(episodes=topEpis,rf_n=20)
#rets = getRets(evo)

#evo_n = EvoNeuro(episodes=topEpis)
#rets_n = getRets(evo_n)

evo_r = EvoRBF(episodes=topEpis)
rets_r = getRets(evo_r)

#%%
#plt.figure()
#plt.plot(np.arange(200),rets)
#plt.plot(np.arange(200), rets_n)
# plt.errorbar(rf_ns, means, stds)

#%%

#episode, t = trainer.run_once(
#        lambda obs:evo_n.getAction(obs,0),
#        max_iter=5000,  
#        render=True
#)