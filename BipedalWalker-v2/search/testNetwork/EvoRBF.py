from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.kernel_approximation import RBFSampler
import numpy as np

class EvoRBF():
    def __init__(self,episodes='data.pkl'):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        for i, episode in enumerate(episodes):
            obs, action = self.get(episode)
            if i == 0:
                observations = obs
                actions = action
            else:
                observations = np.vstack(( \
                    obs,
                    observations
                ))
                actions = np.vstack(( \
                    action,
                    actions
                ))


        self.pipe = Pipeline([
            ('scale', StandardScaler()),
            ('rbf', FeatureUnion([
                # ('rbf5', RBFSampler(gamma=5.0, n_components=100)),
                # ('rbf2', RBFSampler(gamma=2.0, n_components=100)),
                # ('rbf1', RBFSampler(gamma=1.0, n_components=100)),
                ('rbf05', RBFSampler(gamma=0.5, n_components=100)),
            ]))
        ])
        observations = self.pipe.fit_transform(observations)
        print(observations.shape)
        self.model = LinearRegression(fit_intercept=False)
        self.model.fit(observations,actions)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def getAction(self,obs, eps):
        obs = obs[None,:]
        obs = self.pipe.transform(obs)
        action = self.model.predict(obs)[0]
        if np.random.rand() < eps:
            action = np.random.normal(action*1.2, self.action_std*0.2)
        return action
