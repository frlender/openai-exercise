from keras.models import Sequential
from keras.layers import Dense, Activation
import numpy as np

def getModel():
    model = Sequential([
        Dense(240, input_dim=24),
        Activation('relu'),
        Dense(120),
        Activation('relu'),
        Dense(60),
        Activation('relu'),
        Dense(4),
        Activation('tanh'),
    ])
    model.compile(
        optimizer='adam',
        loss='mse',
    )
    return model

class EvoNeuro():
    def __init__(self,episodes='data.pkl'):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        for i, episode in enumerate(episodes):
            obs, action = self.get(episode)
            if i == 0:
                observations = obs
                actions = action
            else:
                observations = np.vstack(( \
                    obs,
                    observations
                ))
                actions = np.vstack(( \
                    action,
                    actions
                ))

        self.model = getModel()
        self.model.fit(observations,actions, epochs=8, batch_size=20)
        self.action_std = np.std(actions, axis=0)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def getAction(self,obs, eps):
        action = self.model.predict(obs[None,:])[0]
        if np.random.rand() < eps:
            action = np.random.normal(action*1.2, self.action_std*0.2)
        return action
