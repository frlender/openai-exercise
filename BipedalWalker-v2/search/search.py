import pa
import match as m
import qn
import matplotlib.pyplot as plt
from Evo import Evo

trainer = pa.Trainer('BipedalWalker-v2')

episodes = []

for i in range(0,int(1e5)):
    episode, t = trainer.run_once(
            lambda obs:trainer.env.action_space.sample()
#            lambda obs:evo.getAction(obs,0.3),
    )
    qn.printEvery(100, i)
    if m.match(episode):
        episodes.append(episode)
        if len(episodes) >= 10:
            break
        print('{} epis'.format(len(episodes)))


#evo = Evo(epis,rf_n=10)


#%%
epis = list(map(m.transform,episodes))
epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
qn.dumpPkl(epis,'data_4.pkl')

#episode = episodes[5]
obs, _ = m.getHipsAngle(epis[1])
m.plot(obs)



#epis = []
#for epi in episodes:
#    epis.append(m.transform(epi))
#
#epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
#
#epiHips = [m.getHipsAngle(epi,0.05)[0] for epi in epis]
#hips, _ = m.getHipsAngle(episode)
#
#obs = epiHips[12]
#x = range(obs.shape[0])
#for i in [0,1]:
#    plt.plot(x,obs[:,i],label=str(i))
#plt.legend()
