import pa
import qn
import matplotlib.pyplot as plt

from Evo import Evo
import match as m
import numpy as np

top = 20
rf_n = 15
session_len = 200
# session_count = 10
eps = lambda x:30/(50+x)

trainer = pa.Trainer('BipedalWalker-v2')
# evo = Evo('data_4.pkl',rf_n=rf_n)
evo = qn.loadPkl('models/120_22.72.pkl')
best = 20

for i in range(120,500):
    episodes = []
    n = 0
    while(len(episodes)<session_len):
        _episodes, ts = trainer.parallel(
            evo,
            eps(i),
            20
        )
        episodes += list(filter(m.match, _episodes))
        print('{}th, {} epis'.format(n,len(episodes)))
        n += 1

    epis = list(map(m.transform,episodes))
    epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
    rets = [m.getMaxReturn(x)[1] for x in epis]
    avgRet =np.mean(rets)
    print('{}th session, avgRet {:.2f}, best ret {:.2f}'.format(
    i+1, avgRet, rets[0]))

    if (i+1) % 50 == 0 or avgRet>best:
        if avgRet > best:
            best = avgRet
        qn.dumpPkl(evo,'models/{}_{:.2f}.pkl'.format(i+1,np.mean(rets)))

    evo = Evo(episodes=epis[:top],rf_n=rf_n)
