import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import match as m

import matplotlib.pyplot as plt

from Evo import Evo


tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
evo = qn.loadPkl('models/310_55.69.pkl')

episodes = []
for i in range(400):
    episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs,0),
        max_iter=5000,
        render=False
    )
    episodes.append(episode)
    
epis = list(map(m.transform,episodes))
epis = sorted(epis, key=lambda x:-m.getMaxReturn(x)[1])
rets = [m.getMaxReturn(x)[1] for x in epis]

#%%
top = 25
rf_ns = [5,10,15,20,25,30,35]
stats = []
for rf_n in rf_ns:
    print(rf_n)
    evo = Evo(episodes=epis[:top],rf_n=rf_n)
    episodes = []
    for i in range(200):
        episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,0),
            max_iter=5000,
            render=False
        )
        episodes.append(episode)
    epis2 = list(map(m.transform,episodes))
    epis2 = sorted(epis2, key=lambda x:-m.getMaxReturn(x)[1])
    rets = [m.getMaxReturn(x)[1] for x in epis2][:20]
    stats.append([np.mean(rets),np.std(rets)])


#%%
means, stds = list(zip(*stats))
plt.figure()
plt.errorbar(rf_ns, means, stds)
