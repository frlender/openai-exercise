import pickle
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
import numpy as np
import qn

class Evo():
    def __init__(self,episodes='data2.pkl', rf_n=15):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        for i, episode in enumerate(episodes):
            obs, action = self.get(episode)
            if i == 0:
                observations = obs
                actions = action
            else:
                observations = np.vstack(( \
                    obs,
                    observations
                ))
                actions = np.vstack(( \
                    action,
                    actions
                ))

        self.model = RandomForestRegressor(n_estimators=rf_n)
        self.model.fit(observations,actions)
        self.action_std = np.std(actions, axis=0)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def getAction(self,obs, eps):
        action = self.model.predict([obs])[0]
        if np.random.rand() < eps:
            action = np.random.normal(action, self.action_std)
        return action
