import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import match as m

import matplotlib.pyplot as plt

from Evo import Evo


tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
evo = qn.loadPkl('../search/models/0.2_std/150_113.22.pkl')

def getAction(obs):
    actionLeft = evo.getAction(obs,0.8)
    obsRight = np.copy(obs)
    obsRight[4:9] = obs[9:14]
    obsRight[9:14] = obs[4:9]
    actionRight = evo.getAction(obsRight,0)
    return np.hstack([actionLeft[:2],actionRight[:2]])

episode, t = trainer.run_once(
        getAction,
        max_iter=5000,  
        render=True
)
ret = m.getMaxReturn(episode)[1]
tracker.isBest(ret)
print("{} timesteps, current {:.3f}."\
.format(t, ret))


#%%
obs, action = evo.get(episode)
#obs, _ = m.getHipsAngle(episode)
m.plot(obs[:,[7,12]])