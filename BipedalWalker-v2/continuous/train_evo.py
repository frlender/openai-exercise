import gym
from gym import wrappers
import qn
import numpy as np
# from Trainer import Trainer
from pa import Trainer


from Evo import Evo
evo = None
avgRet = 0

top = 0.1
rf_n = 15
session_len = 200
session_count = 300
eps = lambda x: np.abs(x-180)/300
max_iter = 2000


trainer = Trainer('BipedalWalker-v2')

episodes = []

for i in range(session_count):
    if evo:
        episodes, ts = trainer.parallel(
            evo,
            eps(avgRet),
            session_len=session_len
        )
    else:
        episodes, ts = trainer.parallel(
            'random',
            eps(avgRet),
            session_len=session_len
        )
    rets = sorted([Evo.getMaxReturn(x)[1] for x in episodes], reverse=True)
    avgRet = np.mean(rets[:int(session_len/2)])
    print('{}th session, avg ret {:.3f}, best ret {:.3f}'
    .format(i+1, avgRet, rets[0]))

    evo = Evo(episodes,top=top,rf_n=rf_n)
    episodes = []

    if (i+1)%10 == 0:
        qn.dumpPkl(evo, 'models/session_{}_top_{}_avg_{:.2f}.pkl'.format(i+1,
        top, avgRet))


#qn.dumpPkl(episodes,'data_e.pkl')
#qn.dumpPkl(evo,'best_evo.pkl')
