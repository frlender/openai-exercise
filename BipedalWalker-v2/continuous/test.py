import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle



tracker = pa.Tracker(greater=True)
trainer = pa.Trainer('BipedalWalker-v2')

#%%
with open('models/session_70_top_0.1_avg_4.7e+01.pkl','rb') as mf:
    evo = pickle.load(mf)

episode, t = trainer.run_once(
        lambda obs:evo.getAction(obs,0.5),
        max_iter=5000,
        render=True
)
ret = evo.getMaxReturn(episode)[1]
tracker.isBest(ret)
print("{} timesteps, current {:.3f}."\
.format(t, ret))


#%%
x = []
for item in episodes:
    x.append(evo.getMaxReturn(item)[1])
x = sorted(x,reverse=True)