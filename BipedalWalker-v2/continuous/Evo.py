import pickle
from sklearn.preprocessing import PolynomialFeatures
from sklearn.ensemble import RandomForestRegressor
from sklearn.pipeline import Pipeline
import numpy as np
import qn
from scipy.stats import mannwhitneyu

class Evo():
    def __init__(self,episodes,top=0.1, rf_n=10):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        sortedEpisodes = self.sortEpisodes(episodes)
        top_episodes = sortedEpisodes[:int(len(sortedEpisodes)*top)]

        print('added {} episodes.'.format(len(top_episodes)))

        for i, episode in enumerate(top_episodes):
            idx = self.getMaxReturn(episode)[0]
            episode = episode[:idx+1]
            obs, action = self.get(episode)
            if i == 0:
                observations = obs
                actions = action
            else:
                observations = np.vstack(( \
                    obs,
                    observations
                ))
                actions = np.vstack(( \
                    action,
                    actions
                ))

        self.model = RandomForestRegressor(n_estimators=rf_n, max_depth=30)
        self.model.fit(observations,actions)
        self.action_std = np.std(actions, axis=0)

    @classmethod
    def getMaxReturn(cls, episode):
        cumsum = np.cumsum([x[2] for x in episode])
        idx = np.argmax(cumsum)
        return idx, cumsum[idx]

    @classmethod
    def getSymmetry(cls, episode, percent=0.1):
        actions = [x[1] for x in episode]
        actions = np.vstack(actions)
        sortedActions = np.sort(actions,axis=0)
        dimLen = sortedActions.shape[0]
        lowerIdx = int(dimLen*percent)
        upperIdx = int(dimLen*(1-percent))+1
        middledActions = sortedActions[lowerIdx:upperIdx,:]
        _, hip_pval = mannwhitneyu(middledActions[:,0], middledActions[:,2])
        _, joint_pval = mannwhitneyu(middledActions[:,1], middledActions[:,3])
        # the large the pval the more similar the distribution.
        stat = - np.mean([hip_pval,joint_pval])
        return stat

    @classmethod
    def sortEpisodes(cls, episodes):
        return qn.rpSort(episodes,[
            lambda x: -cls.getMaxReturn(x)[1],
            # cls.getSymmetry
        ])


    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)


    def getAction(self, obs, eps):
        action = self.model.predict([obs])[0]
        if np.random.rand() < eps:
            action = np.random.normal(action, self.action_std)
        return action
