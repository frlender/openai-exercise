import numpy as np
import qn
import yaml
import gym
from multiprocessing import Pool

class _RandomActionObj():
    def __init__(self, env):
        self.env = env

    def getAction(self, obs, eps):
        return self.env.action_space.sample()

class Trainer():
    def __init__(self,envName):
        self.envName = envName
        self.env = gym.make(envName)

    @staticmethod
    def _run_once(args):
        envName = args[0]
        actionObj = args[1]
        eps = args[2]
        max_iter = args[3]

        env = gym.make(envName, silence=True)
        if isinstance(actionObj,str) and actionObj == 'random':
            actionObj = _RandomActionObj(env)

        episode = []
        actions = []
        obs = env.reset()
        for t in range(int(max_iter)):
            if t%3 == 0:
                action = actionObj.getAction(obs, eps)
            else:
                action = actions[-1]
            # suppose the env will clip action automatically
            newObs, reward, done, info = env.step(action)
            actions.append(action)
            episode.append([obs,action,reward])
            obs = newObs
            if done:
                break
        return episode, t+1

    def parallel(self,actionObj, eps, session_len, cores=6, max_iter=2000,
        every=20):
        params = []
        for i in range(session_len):
            #copiedActionObj = deepcopy(actionObj)
            params.append((self.envName, actionObj, eps, max_iter))

        with Pool(cores) as p:
            res = []
            for i, item in enumerate(
                p.imap_unordered(self._run_once, params)
            ):
                qn.printEvery(every, i+1, '{}/{}'.format(i+1,session_len))
                res.append(item)
        return list(zip(*res))

    def run_once(self,actionFun,max_iter=1e6,tracker=None,
    keep_size=None,add_step=False,force_range=True,render=False):
        episode = []
        obs = self.env.reset()
        for t in range(int(max_iter)):
            if render:
                self.env.render()
            action = actionFun(obs)
            if force_range:
                forced_action = np.clip(action,
                    self.env.action_space.low,
                    self.env.action_space.high)
                newObs, reward, done, info = self.env.step(forced_action)
            else:
                newObs, reward, done, info = self.env.step(action)
            episode.append([obs,action,reward])
            obs = newObs
            if done:
                break

        if tracker:
            tracker.isBest(t+1)

        if add_step:
            for item in episode:
                item.append(t+1)

        if keep_size:
            episode = episode[-keep_size:]

        return episode, t+1
