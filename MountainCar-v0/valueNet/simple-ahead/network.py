import tensorflow as tf

h1Len = 72
h2Len = 48
value_lr = 0.1

with tf.name_scope('input'):
    observation = tf.placeholder(tf.float32, [None,2], 'observation')
    G = tf.placeholder(tf.float32, [None,1], 'target')
    action = tf.placeholder(tf.float32, [None,3], 'action')
    advantage = tf.placeholder(tf.float32, [None,1],'advantage')

with tf.name_scope('value_net'):
    w_v1 = tf.Variable(tf.truncated_normal([2,h1Len], stddev=0.1),
    name='w_v1')
    b_v1 = tf.Variable(tf.constant(0.1, shape=[h1Len]), name='b_v1')
    pa_v1 = tf.matmul(observation, w_v1) + b_v1
    a_v1 = tf.tanh(pa_v1, name='a_v1')

    w_v2 = tf.Variable(tf.truncated_normal([h1Len,h2Len], stddev=0.1),
    name='w_v2')
    b_v2 = tf.Variable(tf.constant(0.1, shape=[h2Len]), name='b_v2')
    pa_v2 = tf.matmul(a_v1, w_v2) + b_v2
    a_v2 = tf.tanh(pa_v2,name='a_v2')

    w_v3 = tf.Variable(tf.truncated_normal([h2Len,1], stddev=0.1),
    name='w_v3')
    b_v3 = tf.Variable(tf.constant(0.1), name='b_v3')
    value = tf.matmul(a_v2, w_v3) + b_v3

    loss = tf.contrib.losses.mean_squared_error(value, G)
    train_value = tf.train.AdamOptimizer(learning_rate=value_lr)\
    .minimize(loss)
