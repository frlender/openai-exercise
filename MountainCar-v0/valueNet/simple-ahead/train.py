import numpy as np
from numpy.matlib import repmat
import tensorflow as tf
import qn
import shutil
import os
import gym
from gym import wrappers
import pa
import rl

import network as q

baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

#saver.restore(sess, baseDir+'models/moutainCar-policy-gradient-pretrain-simple')

env = gym.make('MountainCar-v0')
# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

keep_size = 120
gamma = 0.99
epsilonUp = 15
epsilonDn = 100
batch_size = 5

encoder = rl.OneHotEncoder([0,1,2])

for i_episode in range(int(6e3)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > epsilonUp/(epsilonUp+i_episode):
            action_prob = sess.run(q.action_prob,
            feed_dict={q.observation:[obs]})
            action = rl.getAction(action_prob[0])
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break

    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    observations, actions, rewards = zip(*memory)
    observations = np.array(observations)
    actions = encoder.encode(actions)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets)) + 1

    for chunk in qn.chunks(range(len(memory)),batch_size):
        obsChunk = observations[chunk,:]
        targetChunk = np.array(targets)[chunk]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})
