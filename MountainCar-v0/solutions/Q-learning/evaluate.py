import numpy as np
from random import shuffle
import tensorflow as tf
import shutil
import os
import qn

import gym
from gym import wrappers
import pa

from util import QLearn
import Q as q

baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver.restore(sess, baseDir+'models/moutainCar-train-q-Q-learning-new')

#%%
env = gym.make('MountainCar-v0')
logDir = baseDir+'tmp/MountainCar-v0-q-learning-360'
if os.path.isdir(logDir):
    shutil.rmtree(logDir)
env = wrappers.Monitor(env, logDir)

tracker = pa.Tracker()
ql = QLearn(sess,q)

for i_episode in range(int(500)):
    obs = env.reset()
    for t in range(int(1e10)):
        #env.render()
        #qn.printEvery(1000,t)

        # get action
        action_q = ql.getQ([obs])
        action = np.argmax(action_q[0])
#        if np.random.rand() < 1e-10:
#            action = env.action_space.sample()

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}"\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)
