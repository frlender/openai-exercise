import numpy as np
from random import shuffle
import tensorflow as tf
import shutil
import os
import qn

import gym
from gym import wrappers
import pa

from util import QLearn
import Q as q


baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

suffix = 'new'
saver.restore(sess,
baseDir+'models/moutainCar-pretrain-q-Q-learning-{}'.format(suffix))

# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

keep_size = 1000
sample_size = 200
batch_size = 10

eps = [150,500]
gamma = 0.99

tracker = pa.Tracker()
ql = QLearn(sess,q)
env = gym.make('MountainCar-v0')

for i_episode in range(int(1e4)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        p = eps[0]/(eps[1]+i_episode)
        if seed > p:
            action_q = ql.getQ([obs])
            action = np.argmax(action_q[0])
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}"\
            .format(i_episode, t+1, tracker.best))
            break

    if tracker.isBest(t+1):
        saver.save(sess,
        baseDir+'models/moutainCar-train-q-Q-learning-{}'.format(suffix))


    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    observations, actions, rewards = zip(*memory)
    features = ql.getFeatures(observations,actions)
    maxQ = ql.getMaxQ(observations)
    targets = np.array(rewards) + gamma*np.concatenate((maxQ[1:],[0]))

    idx = list(range(len(memory)))
    shuffle(idx)
    idx = idx[:sample_size]

    for chunk in qn.chunks(idx,batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk][:,None]

        sess.run(q.train_value,feed_dict={q.observation:featureChunk,
        q.target:targetChunk})
