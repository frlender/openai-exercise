import pickle
import numpy as np
from random import shuffle
import rl
import pa

import tensorflow as tf
import network as q
import qn

baseDir = pa.getBaseDir()
with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

gamma = 1
batch_size = 20

encoder = rl.OneHotEncoder([0,1,2])

for epoch in range(2):
    for i,memory in enumerate(train[:2500]):
        qn.printEvery(10,i)
        memory = memory[-500:]
        observations, actions, rewards, _ = zip(*memory)
        observations = np.array(observations)
        #observations[:,1] = 10*observations[:,1]
        actions = encoder.encode(actions)
        targets = np.array(rl.getMC(rewards,gamma))
        targets = targets/np.max(np.abs(targets)) + 1

        idx = list(range(len(memory)))
        shuffle(idx)

        for chunk in qn.chunks(idx,batch_size):
            obsChunk = observations[chunk,:]
            targetChunk = targets[chunk]
            actionChunk = actions[chunk,:]

            sess.run(q.train_action,feed_dict={q.observation:obsChunk,
            q.G:targetChunk, q.action:actionChunk})


action_prob = sess.run(q.action_prob,feed_dict={q.observation:observations})

#%%

saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')
#%%


import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:[obs]})
        if np.random.rand() > 0.1:
            action = np.argmax(action_prob[0])
        else:
            action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
