import numpy as np
import os

import rl
import pa

import tensorflow as tf
import network as q
baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver.restore(sess, baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')

#%%
import gym
from gym import wrappers
env = gym.make('MountainCar-v0')
logDir = baseDir+'tmp/MountainCar-v0-policy-gradient-simple-ahead-120'
if os.path.isdir(logDir):
   shutil.rmtree(logDir)
env = wrappers.Monitor(env, logDir)

tracker = pa.Tracker()
for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        #env.render()
        #qn.printEvery(1000,t)

        # get action
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:[obs]})
        if np.random.rand() > 1e-10:
            action = np.argmax(action_prob[0])
        else:
            action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}"\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)
