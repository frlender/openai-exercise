import pickle
import numpy as np
import rl
import pa

import tensorflow as tf
import network as q
import qn
baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 10

encoder = rl.OneHotEncoder([0,1,2])

mses = []
for i,memory in enumerate(train[:100]):
    qn.printEvery(10,i)
    memory = memory[-200:]
    observations, actions, rewards, _ = zip(*memory)
    observations = np.array(observations)
    actions = encoder.encode(actions)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets)) + 1

#    # modification to avoid minimizing probability to zero
#    actions = (1-actions)/(3-1)
#    targets = - targets


    for chunk in qn.chunks(range(len(memory)),batch_size):
        obsChunk = observations[chunk,:]
        targetChunk = targets[chunk]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})


action_prob = sess.run(q.action_prob,feed_dict={q.observation:observations})
#pa_p2 = sess.run(q.pa_p2,feed_dict={q.observation:observations})
#pa_v2 = sess.run(q.pa_v2,feed_dict={q.observation:observations})
#a_v1 = sess.run(q.a_v1,feed_dict={q.observation:observations})
#
#
#
#w_p2 = sess.run(q.w_p2)
#w_v2 = sess.run(q.w_v2)
#b_p2 = sess.run(q.b_p2)
#b_v2 = sess.run(q.b_v2)
#
#w_p1 = sess.run(q.w_p1)
#w_v1 = sess.run(q.w_v1)
#b_p1 = sess.run(q.b_p1)
#b_v1 = sess.run(q.b_v1)
#

#%%

#saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-split-ahead')
#saver.restore(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-split-ahead')



import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:[obs]})
        action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
