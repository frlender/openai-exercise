import tensorflow as tf

middleLen = 12
target_lr = 0.01

with tf.name_scope('input'):
    observation = tf.placeholder(tf.float32, [None,2], 'observation')
    G = tf.placeholder(tf.float32, [None,1], 'target')
    action = tf.placeholder(tf.float32, [None,3], 'action')

with tf.name_scope('position_net'):
    w_p1 = tf.Variable(tf.truncated_normal([1,middleLen], stddev=0.1),
    name='w_p1')
    b_p1 = tf.Variable(tf.constant(0.1, shape=[middleLen]), name='b_p1')
    pa_p1 = tf.matmul(observation[:,0,None], w_p1) + b_p1
    a_p1 = tf.nn.tanh(pa_p1, name='a_p1')

    w_p2 = tf.Variable(tf.truncated_normal([middleLen,3], stddev=0.1),
    name='w_p2')
    b_p2 = tf.Variable(tf.constant(0.1, shape=[3]), name='b_p2')
    pa_p2 = tf.matmul(a_p1, w_p2) + b_p2

with tf.name_scope('velocity_net'):
    w_v1 = tf.Variable(tf.truncated_normal([1,middleLen], stddev=0.1),
    name='w_v1')
    b_v1 = tf.Variable(tf.constant(0.1, shape=[middleLen]), name='b_v1')
    pa_v1 = tf.matmul(observation[:,1,None], w_v1) + b_v1
    a_v1 = tf.nn.tanh(pa_v1, name='a_v1')

    w_v2 = tf.Variable(tf.truncated_normal([middleLen,3], stddev=0.1),
    name='w_v2')
    b_v2 = tf.Variable(tf.constant(0.1, shape=[3]), name='b_v2')
    pa_v2 = tf.matmul(a_v1, w_v2) + b_v2


with tf.name_scope('action_net'):
    action_prob_p = tf.nn.softmax(pa_p2, name='action_prob_p')
    action_prob_v = tf.nn.softmax(pa_v2, name='action_prob_v')
    action_prob = (action_prob_p+action_prob_v)/2


    with tf.name_scope('gradient'):
        chosen_action_prob = tf.reduce_sum(tf.multiply(action_prob, action),
        axis=1, keep_dims=True)
        log_prob = tf.log(chosen_action_prob)
        J = tf.multiply(log_prob, G)
        loss = - tf.reduce_mean(J)
        train_action = tf.train.AdamOptimizer(learning_rate=target_lr)\
        .minimize(loss)
