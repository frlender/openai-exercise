import tensorflow as tf

positionCount = 40
velocityCount = 20
convergeCount = 30
value_lr = 0.1
action_lr = 0.01


with tf.name_scope('input'):
    position = tf.placeholder(tf.float32, [None,positionCount], 'position')
    velocity = tf.placeholder(tf.float32, [None,velocityCount], 'velocity')
    target = tf.placeholder(tf.float32, [None,1], 'target')
    action = tf.placeholder(tf.float32, [None,3], 'action')


with tf.name_scope('position'):
    w_p1 = tf.Variable(tf.truncated_normal([positionCount,convergeCount], stddev=0.1),
    name='w_p1')
    b_p1 = tf.Variable(tf.constant(0.1, shape=[convergeCount]), name='b_p1')
    pa_p1 = tf.matmul(position, w_p1) + b_p1

with tf.name_scope('velocity'):
    w_v1 = tf.Variable(tf.truncated_normal([velocityCount,convergeCount], stddev=0.1),
    name='w_v1')
    b_v1 = tf.Variable(tf.constant(0.1, shape=[convergeCount]), name='b_v1')
    pa_v1 = tf.matmul(velocity, w_v1) + b_v1

with tf.name_scope('converge'):
    a_converge = tf.nn.relu(pa_p1+pa_v1)

with tf.name_scope('value'):
    w_value1 = tf.Variable(tf.truncated_normal([convergeCount,1],
    stddev=0.1), name='w_value1')
    b_value1 = tf.Variable(tf.constant(0.1, shape=[1]),
    name='b_value1')
    pa_value1 = tf.matmul(a_converge, w_value1) + b_value1

    value = pa_value1
    mse = tf.contrib.losses.mean_squared_error(value, target)
    train_value = tf.train.AdamOptimizer(learning_rate=value_lr)\
    .minimize(mse)

with tf.name_scope('action'):
    w_a1 = tf.Variable(tf.truncated_normal([convergeCount,3], stddev=0.1),
    name='w_a1')
    b_a1 = tf.Variable(tf.constant(0.1, shape=[3]), name='b_a1')
    pa_a1 = tf.matmul(a_converge, w_a1) + b_a1
    action_prob = tf.nn.softmax(pa_a1, name='action_prob')

    chosen_action_prob = tf.reduce_sum(tf.multiply(action_prob, action),
    axis=1, keep_dims=True)
    normalized = chosen_action_prob + 1e-30
    log_prob = tf.log(normalized)
    J = tf.multiply(log_prob, target-value)
    loss = - tf.reduce_mean(J)
    train_action = tf.train.AdamOptimizer(learning_rate=action_lr)\
    .minimize(loss)
