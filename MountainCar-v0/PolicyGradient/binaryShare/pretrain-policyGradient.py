import pickle
import numpy as np
import tensorflow as tf

import gym
import qn
import rl
import pa
baseDir = pa.getBaseDir()

with open(baseDir+'data/train.pkl','rb') as mf:
    train = pickle.load(mf)


#%%
import policyGradient as q

env = gym.make('MountainCar-v0')
encoder = pa.AsymetricSpaceEncoder(env,
[ [ [[-1.2,-0.8],8],[[-0.8,-0.2],20],[[-0.2,0.6],12] ],
 [ [[-0.07,0.07],20] ] ])

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

mses = []
losses = []
batch_size = 50
for i,memory in enumerate(train[:500]):
    qn.printEvery(100,i)
    observations, actions, rewards, _ = zip(*memory)
    targets = rl.getMC(rewards)
    position,velocity,actionFeature = \
    encoder.encodeObsActionSeperate(observations,actions)
    
#    j = 0
    for chunk in qn.chunks(range(len(memory)),batch_size):
#        if j== 30:
#            break
        positionChunk = position[chunk,:]
        velocityChunk = velocity[chunk,:]
        actionChunk = actionFeature[chunk,:]
        targetChunk = np.array(targets)[chunk]
        
#        loss = sess.run(q.loss,feed_dict= \
#        {q.position:positionChunk, q.velocity:velocityChunk,
#         q.action:actionChunk, q.target:targetChunk})
#        losses.append(loss)
#        if np.isinf(loss):
#            break
         
        sess.run(q.train_action,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk,
         q.action:actionChunk, q.target:targetChunk})
        
        sess.run(q.train_value,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk,
         q.target:targetChunk})
        
        mse = sess.run(q.mse,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk,
         q.target:targetChunk})
        mses.append(mse)
        
#        j+=1

#print(mses)
#print(losses)
#
#w_a1 = sess.run(q.w_a1,feed_dict= \
#        {q.position:positionChunk, q.velocity:velocityChunk,
#         q.action:actionChunk, q.target:targetChunk})
#
pa_a1 = sess.run(q.pa_a1,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk,
         q.action:actionChunk, q.target:targetChunk})

action_prob = sess.run(q.action_prob,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk,
         q.action:actionChunk, q.target:targetChunk})
#
#normalized = sess.run(q.normalized,feed_dict= \
#        {q.position:positionChunk, q.velocity:velocityChunk,
#         q.action:actionChunk, q.target:targetChunk})
#
#log_prob = sess.run(q.log_prob,feed_dict= \
#        {q.position:positionChunk, q.velocity:velocityChunk,
#         q.action:actionChunk, q.target:targetChunk})

import matplotlib.pyplot as plt
plt.plot(mses)
plt.figure()
val = sess.run(q.value,feed_dict= \
        {q.position:positionChunk, q.velocity:velocityChunk})
plt.plot(val[:,0][:200])
#%%
saver.save(sess,baseDir+'models/moutainCar-pretrain-place-code-asymetric-plicy-gradient')
#%%
saver.restore(sess,baseDir+'models/moutainCar-pretrain-place-code-asymetric-plicy-gradient')


for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.1:
            position,velocity = encoder.encodeObsSeperate([obs])
            action_p = sess.run(q.action_prob,feed_dict=\
                                {q.position:position,q.velocity:velocity})
            action = rl.getAction(action_p[0])
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
