import tensorflow as tf

h1Len = 48
h2Len = 48
h3Len = 48
activate = tf.nn.relu
target_lr = 0.01

with tf.name_scope('input'):
    observation = tf.placeholder(tf.float32, [None,2], 'observation')
    G = tf.placeholder(tf.float32, [None,1], 'target')
    action = tf.placeholder(tf.float32, [None,3], 'action')


with tf.name_scope('action_net'):
    w_a1 = tf.Variable(tf.truncated_normal([2,h1Len], stddev=0.1),
    name='w_a1')
    b_a1 = tf.Variable(tf.constant(0.1, shape=[h1Len]), name='b_a1')
    pa_a1 = tf.matmul(observation, w_a1) + b_a1
    a_a1 = activate(pa_a1, name='a_a1')

    w_a2 = tf.Variable(tf.truncated_normal([h1Len,h2Len], stddev=1),
    name='w_a2')
    b_a2 = tf.Variable(tf.constant(0.1, shape=[h2Len]), name='b_a2')
    pa_a2 = tf.matmul(a_a1, w_a2) + b_a2
    a_a2 = activate(pa_a2, name='a_a2')

    w_a3 = tf.Variable(tf.truncated_normal([h2Len,h3Len], stddev=1),
    name='w_a3')
    b_a3 = tf.Variable(tf.constant(0.1, shape=[h3Len]), name='b_a3')
    pa_a3 = tf.matmul(a_a2, w_a3) + b_a3
    a_a3 = activate(pa_a3, name='a_a3')


    w_a4 = tf.Variable(tf.truncated_normal([h3Len,3], stddev=1),
    name='w_a4')
    b_a4 = tf.Variable(tf.constant(0.1, shape=[3]), name='b_a4')
    pa_a4 = tf.matmul(a_a3, w_a4) + b_a4
    action_prob = tf.nn.softmax(pa_a4, name='action_prob')

    with tf.name_scope('gradient'):
        chosen_action_prob = tf.reduce_sum(tf.multiply(action_prob, action),
        axis=1, keep_dims=True)
        log_prob = tf.log(chosen_action_prob)
        J = tf.multiply(log_prob, G)
        loss = - tf.reduce_mean(J)
        train_action = tf.train.AdamOptimizer(learning_rate=target_lr)\
        .minimize(loss)
