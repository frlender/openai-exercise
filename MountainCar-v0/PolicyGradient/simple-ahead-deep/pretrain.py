import pickle
import numpy as np
import rl
import pa

import tensorflow as tf
import network as q
import qn
baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 10

encoder = rl.OneHotEncoder([0,1,2])

mses = []
for i,memory in enumerate(train[:500]):
    qn.printEvery(10,i)
    memory = memory[-200:]
    observations, actions, rewards, _ = zip(*memory)
    observations = np.array(observations)
    actions = encoder.encode(actions)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets)) + 1

#    # modification to avoid minimizing probability to zero
#    actions = (1-actions)/(3-1)
#    targets = - targets


    for chunk in qn.chunks(range(len(memory)),batch_size):
        obsChunk = observations[chunk,:]
        targetChunk = targets[chunk]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})


action_prob = sess.run(q.action_prob,feed_dict={q.observation:observations})

#%%

#saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')
#saver.restore(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')



import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:[obs]})
        action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
