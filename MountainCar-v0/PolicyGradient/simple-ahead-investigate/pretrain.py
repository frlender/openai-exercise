import pickle
import numpy as np
import rl
import pa

import tensorflow as tf
import network as q
import qn
baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)
    

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 1

encoder = rl.OneHotEncoder([0,1,2])

mses = []
bs = [sess.run(q.b_a2)]
gradientsAll = []
for i,memory in enumerate(train[:1]):
    qn.printEvery(10,i)
    memory = memory[-200:]
    observations, actions, rewards, _ = zip(*memory)
    observations = np.array(observations)
    actions = encoder.encode(actions)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets))

#    # modification to avoid minimizing probability to zero
#    actions = (1-actions)/(3-1)
#    targets = - targets


    for chunk in qn.chunks(range(len(memory)),batch_size):
        obsChunk = observations[chunk,:]
        targetChunk = targets[chunk]
        actionChunk = actions[chunk,:]
        
        gradients = sess.run(q.gradients,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})
        gradientsAll.append(gradients[1])
        
        sess.run(q.train_action,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})
        
        bs.append(sess.run(q.b_a2))


bs = np.stack(bs)
        
action_prob = sess.run(q.action_prob,feed_dict={q.observation:observations})
pa_a2 = sess.run(q.pa_a2,feed_dict={q.observation:observations})

w = sess.run(q.w_a2)
b = sess.run(q.b_a2)

import matplotlib.pyplot as plt
plt.plot(bs[:,0])
plt.plot(bs[:,1])
plt.plot(bs[:,2])

#%%

saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')
#saver.restore(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')



import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:[obs]})
        action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
