import numpy as np
from numpy.matlib import repmat
from random import shuffle
import tensorflow as tf
import qn
import shutil
import os
import gym
from gym import wrappers
import pa
import rl

import network as q

baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

saver.restore(sess, baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')

env = gym.make('MountainCar-v0')
# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

keep_size = 300
sample_size = 300
batch_size = 20

eps = [150,500]
gamma = 1

encoder = rl.OneHotEncoder([0,1,2])
tracker = pa.Tracker()

for i_episode in range(int(1e4)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        action_prob = sess.run(q.action_prob,
        feed_dict={q.observation:[obs]})
        seed = np.random.rand()
        if seed > eps[0]/(eps[1]+i_episode):
            action = rl.getAction(action_prob[0])
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}, batch_size {}"\
            .format(i_episode, t+1, tracker.best, batch_size))
            break

    if tracker.isBest(t+1):
        saver.save(sess,baseDir+'models/moutainCar-policy-gradient-train-simple-ahead')

    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    observations, actions, rewards = zip(*memory)
    observations = np.array(observations)
    #observations[:,1] = 10*observations[:,1]
    actions = encoder.encode(actions)
    targets = np.array(rl.getMC(rewards,gamma))
    targets = targets/np.max(np.abs(targets)) + 1


    idx = list(range(len(memory)))
    shuffle(idx)
    idx = idx[:sample_size]

    for chunk in qn.chunks(idx,batch_size):
        obsChunk = observations[chunk,:]
        targetChunk = targets[chunk,:]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.observation:obsChunk,
        q.G:targetChunk, q.action:actionChunk})
