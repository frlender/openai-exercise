import pickle
import numpy as np
import rl
import pa
import gym

import tensorflow as tf
import network as q
import qn
baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

env = gym.make('MountainCar-v0')
encoder = pa.SpaceEncoder(env, [50,20])

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 10

actionEncoder = rl.OneHotEncoder([0,1,2])

mses = []
for i,memory in enumerate(train[:2500]):
    qn.printEvery(10,i)
    memory = memory[-200:]
    observations, actions, rewards, _ = zip(*memory)
    features = encoder.encodeObs(observations)
    actions = actionEncoder.encode(actions)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets)) + 1


    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.feature:featureChunk,
        q.G:targetChunk, q.action:actionChunk})


action_prob = sess.run(q.action_prob,feed_dict={q.feature:features})

#%%

#saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead-binary')
#saver.restore(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead-binary')



env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        feature = encoder.encodeObs([obs])
        action_prob = sess.run(q.action_prob,feed_dict={q.feature:feature})
        action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
