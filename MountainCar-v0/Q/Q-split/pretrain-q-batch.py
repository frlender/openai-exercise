import pickle
import numpy as np
import rl as u
import pa

# with open('data/mountain-car-1.pkl','rb') as mf:
#     total = pickle.load(mf)
#
# keep_size = 2000
# gamma = 1
#
# train = []
# for memory in total:
#     if len(memory) > keep_size:
#         memory = memory[-keep_size:]
#
#     for i, unit in enumerate(memory):
#         G = np.sum([x[2] for x in memory[i:]])
#         unit.append(G)
#     train.append(memory)
#
# with open('data/train.pkl','wb') as mf:
#      pickle.dump(train,mf)
baseDir = pa.getBaseDir()
with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%
import tensorflow as tf
#import Q as q
import Q as q
import qn


saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

mses = []
batch_size = 10
gamma = 1
for i,memory in enumerate(train[:2500]):
    qn.printEvery(10,i)
    memory = memory[-200:]
    memoryByAction = [[],[],[]]
    for item in memory:
        memoryByAction[item[1]].append(item)

    for i,actionMemory in enumerate(memoryByAction):
        observations, _, rewards, _ = zip(*actionMemory)
        observations = np.array(observations)
        targets = u.getMC(rewards,gamma)
        targets = targets/np.max(np.abs(targets))
        net = q.nets[i]
        
        for chunk in qn.chunks(range(len(actionMemory)),batch_size):
            obsChunk = observations[chunk,:]
            targetChunk = targets[chunk,:]

            mses.append(sess.run(net['mse'],feed_dict={q.observation:obsChunk,
            q.target:targetChunk}))
            sess.run(net['train'],feed_dict={q.observation:obsChunk,
            q.target:targetChunk})


    # for chunk in qn.chunks(range(len(memory)),batch_size):
    #     obsChunk = tObs[chunk,:]
    #     targetChunk = np.array(targets)[chunk]
    #
    #     mses.append(sess.run(q.mse,feed_dict={q.observation:obsChunk,
    #     q.target:targetChunk}))
    #     sess.run(q.train_value,feed_dict={q.observation:obsChunk,
    #     q.target:targetChunk})



import matplotlib.pyplot as plt
plt.plot(mses)
# plt.figure()
# plt.plot(q_val[:,0][-200:])
#%%
saver.save(sess,baseDir+'models/moutainCar-pretrain-q-split')
#%%
import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.1:
            action_q = []
            for net in q.nets:
                thisQ = sess.run(net['value'],
                feed_dict={q.observation:[obs]})
                action_q.append(thisQ[0][0])
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
