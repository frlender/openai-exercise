import numpy as np
import tensorflow as tf
import Q as q
#import Q2 as q
import qn

import gym
from gym import wrappers
import shutil
import os
import pa
import rl

baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

saver.restore(sess, baseDir+'models/moutainCar-pretrain-q')

env = gym.make('MountainCar-v0')
# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

keep_size = 200
#gamma = 0.99
epsilonUp = 20
epsilonDn = 100
batch_size = 50

tracker = pa.Tracker()

for i_episode in range(int(6e3)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > epsilonUp/(epsilonUp+i_episode):
            action_q = [sess.run(q.value,feed_dict={q.observation:[list(obs)+[0,0,0,0]]})[0][0],
            sess.run(q.value,feed_dict={q.observation:[[0,0]+list(obs)+[0,0]]})[0][0],
            sess.run(q.value,feed_dict={q.observation:[[0,0]+[0,0]+list(obs)]})[0][0]]
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}, batch_size {}"\
            .format(i_episode, t+1, tracker.best, batch_size))
            break

    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    if tracker.isBest(t+1):
        saver.save(sess,baseDir+'models/moutainCar-Q-train')


    tObs = np.zeros((len(memory),len(memory[0][0])*3))
    rewards = []
    for i, item in enumerate(memory):
        rewards.append(item[2])
        action = item[1]
        tObs[i,action*2:action*2+2] = item[0]
    q_val = sess.run(q.value,feed_dict={q.observation:tObs})
    targets = rl.getMC(rewards)
    targets = targets/np.max(np.abs(targets))


    for chunk in qn.chunks(range(len(memory)),batch_size):
        obsChunk = tObs[chunk,:]
        targetChunk = np.array(targets)[chunk]

        sess.run(q.train_value,feed_dict={q.observation:obsChunk,
        q.target:targetChunk})
