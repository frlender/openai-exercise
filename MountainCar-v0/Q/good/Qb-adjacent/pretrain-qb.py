import pickle
import numpy as np
from numpy.matlib import repmat
import tensorflow as tf

import gym
import qn
import rl
import pa


baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

env = gym.make('MountainCar-v0')

#%%
import Qb as q
# feature size 20*10*3 = 600
encoder = pa.SpaceEncoder(env, [40,20], overlap=2)
#2 works best

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

mses = []
batch_size = 40
for i,memory in enumerate(train[:2500]):
    qn.printEvery(100,i)
    memory = memory[-200:]
    observations, actions, rewards, _ = zip(*memory)
    targets = rl.getMC(rewards)
    targets = np.array(targets)
    features = encoder.encodeObsActionAdjacent(observations,actions)
    q_val = sess.run(q.value,feed_dict={q.feature:features})


    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk,:]

        mses.append(sess.run(q.mse,feed_dict={q.feature:featureChunk,
        q.target:targetChunk}))
        sess.run(q.train_value,feed_dict={q.feature:featureChunk,
        q.target:targetChunk})



import matplotlib.pyplot as plt
plt.plot(mses)
plt.figure()
plt.plot(q_val[:,0])
#%%
saver.save(sess,baseDir+'models/moutainCar-pretrain-place-code-2')
#%%

env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.1:
            feature = encoder.encodeObsActionAdjacent(repmat(obs,3,1),[0,1,2])
            action_q = sess.run(q.value,feed_dict={q.feature:feature})
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
