import numpy as np


class QLearn():
    def __init__(self,sess,q):
        self.sess = sess
        self.q = q

    def encode(self,obs,action):
        if action == 0:
            return [list(obs)+[0,0,0,0]]
        elif action == 1:
            return [[0,0]+list(obs)+[0,0]]
        else:
            return [[0,0]+[0,0]+list(obs)]

    def getFeatures(self,observations,actions):
        observations = np.array(observations)
        obsCount = observations.shape[0]
        res = np.zeros((obsCount,6))
        for i in range(obsCount):
            res[i,:] = np.array(
            self.encode(observations[i,:],actions[i]))
        return res

    def getQ(self,observations):
        observations = np.array(observations)
        obsCount = observations.shape[0]
        res = np.zeros((obsCount*3,6))
        for i in range(obsCount):
            for action in [0,1,2]:
                res[i*3+action,:] = np.array(
                self.encode(observations[i,:],action))
        qvals = self.sess.run(self.q.value,
        feed_dict={self.q.observation:res,self.q.keep_prob:1})
        #print(qvals)
        return np.reshape(qvals,(obsCount,3))

    def getMaxQ(self,observations):
        return self.getQ(observations).max(axis=1)
