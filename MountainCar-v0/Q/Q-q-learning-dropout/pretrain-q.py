import pickle
import numpy as np
import pa
from random import shuffle

baseDir = pa.getBaseDir()
with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%
import tensorflow as tf
import Q as q
import qn
from util import QLearn

gamma = 0.99
batch_size = 10
keep_prob = 0.8

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

ql = QLearn(sess,q)


mses = []
for i,memory in enumerate(train[:2500]):
    qn.printEvery(10,i)
    memory = memory[-500:]
    observations, actions, rewards, _ = zip(*memory)
    features = ql.getFeatures(observations,actions)
    maxQ = ql.getMaxQ(observations)
    targets = rewards + gamma*np.concatenate((maxQ[1:],[0]))

    idx = list(range(len(memory)))
    shuffle(idx)

    for chunk in qn.chunks(idx,batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk][:,None]

        mses.append(sess.run(q.mse,feed_dict={q.observation:featureChunk,
        q.target:targetChunk,q.keep_prob:1.0}))
        sess.run(q.train_value,feed_dict={q.observation:featureChunk,
        q.target:targetChunk,q.keep_prob:keep_prob})





import matplotlib.pyplot as plt
plt.plot(mses)
# plt.figure()
# plt.plot(q_val[:,0][-200:])
#%%
saver.save(sess,baseDir+'models/moutainCar-pretrain-q-Q-learning-dropout')
#%%
import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.2:
            action_q = ql.getQ([obs])
            action = np.argmax(action_q[0])
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
