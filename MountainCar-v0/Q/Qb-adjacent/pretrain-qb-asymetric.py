import pickle
import numpy as np
from numpy.matlib import repmat
import tensorflow as tf

import gym
import qn
import rl
import pa


baseDir = pa.getBaseDir()

with open(baseDir+'data/train.pkl','rb') as mf:
    train = pickle.load(mf)

env = gym.make('MountainCar-v0')

#%%
import Qb as q
#import Qb2 as q
# feature size 20*10*3 = 600
encoder = pa.AsymetricSpaceEncoder(env,
[ [ [[-1.2,-0.8],8],[[-0.8,-0.2],20],[[-0.2,0.6],12] ],
 [ [[-0.07,0.07],20] ] ])

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

mses = []
batch_size = 50
for i,memory in enumerate(train):
    qn.printEvery(100,i)
    observations, actions, rewards, _ = zip(*memory)
    targets = rl.getMC(rewards)
    features = encoder.encodeObsAction(observations,actions)
    q_val = sess.run(q.value,feed_dict={q.feature:features})


    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = np.array(targets)[chunk]

        mses.append(sess.run(q.mse,feed_dict={q.feature:featureChunk,
        q.target:targetChunk}))
        sess.run(q.train_value,feed_dict={q.feature:featureChunk,
        q.target:targetChunk})



import matplotlib.pyplot as plt
plt.plot(mses)
plt.figure()
plt.plot(q_val[:,0][:200])
#%%
#saver.save(sess,baseDir+'models/moutainCar-pretrain-place-code-asymetric-2x-full')
#%%
saver.restore(sess,baseDir+'models/moutainCar-pretrain-place-code-asymetric-2x-full')


for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.1:
            feature = encoder.encodeObsAction(repmat(obs,3,1),[0,1,2])
            action_q = sess.run(q.value,feed_dict={q.feature:feature})
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
