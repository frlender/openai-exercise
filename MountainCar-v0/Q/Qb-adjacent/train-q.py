import numpy as np
from numpy.matlib import repmat
import tensorflow as tf
import qn
import shutil
import os
import gym
from gym import wrappers
import pa
import rl

import Qb as q

baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

saver.restore(sess, baseDir+'models/moutainCar-pretrain-place-code-2')

env = gym.make('MountainCar-v0')
# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

keep_size = 200
#gamma = 0.99
epsilonUp = 20
epsilonDn = 100
batch_size = 40

encoder = pa.SpaceEncoder(env, [40,20], overlap=2)

for i_episode in range(int(6e3)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > epsilonUp/(epsilonUp+i_episode):
            feature = encoder.encodeObsActionAdjacent(repmat(obs,3,1),[0,1,2])
            action_q = sess.run(q.value,feed_dict={q.feature:feature})
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break

    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    observations, actions, rewards = zip(*memory)
    targets = rl.getMC(rewards)
    targets = np.array(targets)
    features = encoder.encodeObsActionAdjacent(observations,actions)

    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk,:]

        sess.run(q.train_value,feed_dict={q.feature:featureChunk,
        q.target:targetChunk})
