import pickle
import numpy as np
from random import shuffle
import rl
import pa

import tensorflow as tf
import network as q
import qn
import ware as w

baseDir = pa.getBaseDir()
config = pa.getConfig()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = config['batch_size']

encoder = rl.OneHotEncoder([0,1,2])

for epoch in range(config['epoch']):
    for i,memory in enumerate(train[:2500]):
        qn.printEvery(10,i)
        memory = memory[-500:]
        observations, actions, rewards, _ = zip(*memory)
        features = w.getFeatures(observations)

        actions = encoder.encode(actions)
        targets = np.array(rl.getMC(rewards))
        targets = targets/np.max(np.abs(targets)) + 1

        idx = list(range(len(memory)))
        shuffle(idx)

        for chunk in qn.chunks(idx,batch_size):
            obsChunk = features[chunk,:]
            targetChunk = targets[chunk]
            actionChunk = actions[chunk,:]

            sess.run(q.train_action,feed_dict={q.observation:obsChunk,
            q.G:targetChunk, q.action:actionChunk})



#%%
action_prob = sess.run(q.action_prob,feed_dict={q.observation:features})
#saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')
#%%


import gym
env = gym.make('MountainCar-v0')

tracker = pa.Tracker()
for i_episode in range(500):
    obs = env.reset()
    for t in range(int(300)):
        features = w.getFeatures([obs])
        action_prob = sess.run(q.action_prob,feed_dict={q.observation:features})
        if np.random.rand() > 1e-10:
            action = np.argmax(action_prob[0])
        else:
            action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}"\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)

import yaml
with open('result.yml','a') as rf:
    config['res'] = int(tracker.best)
    yaml.dump(config,rf)
