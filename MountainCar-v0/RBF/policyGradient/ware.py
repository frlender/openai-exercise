import numpy as np
import pickle
from random import shuffle

import pa

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler

baseDir = pa.getBaseDir()
config = pa.getConfig()

with open(baseDir+'data/rbf-fit.pkl','rb') as mf:
    fitData = pickle.load(mf)

pipe = Pipeline([
    ('scale', StandardScaler()),
    ('rbf', RBFSampler(gamma=config['gamma'],
                       n_components=config['feature_size']))
])

shuffle(fitData)
pipe.fit(fitData)

def getFeatures(observations):
    observations = np.array(observations)
    features = pipe.transform(observations)
    return features
