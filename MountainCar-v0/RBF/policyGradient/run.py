import os
import yaml
with open('input.yml','r') as rf:
    configs = yaml.load(rf)

for config in configs:
    with open('config.yml','w') as cf:
        yaml.dump(config,cf)
    os.system('python pretrain.py')
