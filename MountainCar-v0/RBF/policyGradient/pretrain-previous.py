import pickle
import numpy as np
import rl
import pa

import tensorflow as tf
import network2 as q
import qn
import ware as w

baseDir = pa.getBaseDir()

with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

#%%

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 10
keep_size = 200
gamma = 1

encoder = rl.OneHotEncoder([0,1,2])

mses = []
for i,memory in enumerate(train[:2500]):
    qn.printEvery(10,i)
    memory = memory[-keep_size:]
    observations, actions, rewards, _ = zip(*memory)
    actions = encoder.encode(actions)
    features = w.getFeatures(observations)
    targets = np.array(rl.getMC(rewards))
    targets = targets/np.max(np.abs(targets)) + 1


    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk]
        actionChunk = actions[chunk,:]

        sess.run(q.train_action,feed_dict={q.feature:featureChunk,
        q.G:targetChunk, q.action:actionChunk})


action_prob = sess.run(q.action_prob,feed_dict={q.feature:features})

#%%

#saver.save(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')
#saver.restore(sess,baseDir+'models/moutainCar-policy-gradient-pretrain-simple-ahead')



import gym
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        features = w.getFeatures([obs])
        action_prob = sess.run(q.action_prob,feed_dict={q.feature:features})
        action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
