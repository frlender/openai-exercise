import numpy as np
import ware as w
import gym


count = 20
env = gym.make('MountainCar-v0')
spans = np.stack([env.observation_space.low,
        env.observation_space.high]).T
grid = np.meshgrid(np.linspace(spans[0,0],spans[0,1], count),
                   np.linspace(spans[1,0],spans[1,1], count))

obs = np.stack((np.ravel(grid[0]),np.ravel(grid[1]))).T
fs = w.getFeatures(obs)

x = {'a':'b','c':5}
import yaml
with open('x.txt','a') as xf:
    yaml.dump(x,xf)