import numpy as np
import rl
import pa
import ware as w

import tensorflow as tf
import network as q
baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver.restore(sess, baseDir+'models/moutainCar-rbf-policy-gradient-train-simple-ahead')

#%%
import gym
env = gym.make('MountainCar-v0')

tracker = pa.Tracker()
for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #qn.printEvery(1000,t)

        # get action
        feature = w.getFeatures([obs])
        action_prob = sess.run(q.action_prob,
        feed_dict={q.feature:feature})
        if np.random.rand() > 0.1:
            #action = rl.getAction(action_prob[0])
            action = np.argmax(action_prob[0])
        else:
            action = rl.getAction(action_prob[0])

        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}"\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)
