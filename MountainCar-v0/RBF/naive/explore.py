import pa
import pickle
import matplotlib.pyplot as plt
import numpy as np
from sklearn.cluster import KMeans
import yaml

baseDir = pa.getBaseDir()


with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

data = []

for memory in train:
    for item in memory[-200:]:
        data.append(item[0])
        
idx = list(range(len(data)))
sampleIdx = np.random.choice(idx,3000)
samples = np.array([data[x] for x in sampleIdx])
samples[:,1] = 10*samples[:,1]

kmeans = KMeans(n_clusters=50).fit(samples)
centers = kmeans.cluster_centers_


fig, ax = plt.subplots()
plt.scatter(samples[:,0], samples[:,1],s=1)


plt.scatter(centers[:,0],centers[:,1],s=20,c='red')

def getD(gamma):
    return np.sqrt(-np.log(0.1)/gamma)

d = getD(4e2)
for ct in centers:
    circle = plt.Circle(ct,d,color='yellow',alpha=0.2)
    ax.add_artist(circle)

with open('centers.yml','w') as cf:
    yaml.dump(centers.tolist(),cf)