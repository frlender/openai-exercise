import numpy as np
from numpy.matlib import repmat
import yaml

with open('centers.yml') as cf:
    centers = yaml.load(cf)

def getFeature(obs,gamma):
    centerCount = len(centers)
    obsCount = obs.shape[0]
    obsRep = repmat(obs,1,centerCount)
    centerRep = repmat(np.ravel(centers),obsCount,1)
    dist = obsRep-centerRep
    dist = np.reshape(dist,[obsCount,centerCount,2])
    sqrDist = np.sum(dist**2,axis=2)
    return np.exp(-gamma*sqrDist)
