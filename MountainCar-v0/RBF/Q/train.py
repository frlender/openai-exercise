import pickle
import numpy as np
from numpy.matlib import repmat
import pa
import gym

import tensorflow as tf
import qn
import ware as w
from ware import q


baseDir = pa.getBaseDir()
tracker = pa.Tracker()


saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

saver.restore(sess,baseDir+'models/moutainCar-pretrain-rbf-q-2')

env = gym.make('MountainCar-v0')
# logDir = 'tmp/MountainCar-v0-mc'
# if os.path.isdir(logDir):
#     shutil.rmtree(logDir)
# env = wrappers.Monitor(env, logDir)

batch_size = 10
keep_size = 200
gamma = 1
epsilonUp = 30
epsilonDn = 100


for i_episode in range(int(6e3)):
    obs = env.reset()
    memory = []
    for t in range(int(1e10)):
        #env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > epsilonUp/(epsilonUp+i_episode):
            feature = w.getFeatures(repmat(obs,3,1),[0,1,2])
            action_q = sess.run(q.value,feed_dict={q.feature:feature})
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        memory.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}, batch_size {}"\
            .format(i_episode, t+1, tracker.best, batch_size))
            break

    if tracker.isBest(t+1):
        saver.save(sess,baseDir+'models/moutainCar-rbf-train-q-2')

    if len(memory) > keep_size:
        memory = memory[-keep_size:]

    observations, actions, rewards = zip(*memory)
    targets = w.getTargets(rewards,gamma)
    features = w.getFeatures(observations,actions)

    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk,:]

        sess.run(q.train_value,feed_dict={q.feature:featureChunk,
        q.target:targetChunk})
