import pickle
import numpy as np
from numpy.matlib import repmat
import pa
import gym

import tensorflow as tf
import qn
import ware as w
from ware import q


baseDir = pa.getBaseDir()
with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)
#%%
saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())

batch_size = 10
keep_size = 200
gamma = 1

mses=[]
for i,memory in enumerate(train[:2500]):
    qn.printEvery(10,i)
    memory = memory[-keep_size:]
    observations, actions, rewards, _ = zip(*memory)
    targets = w.getTargets(rewards,gamma)
    features = w.getFeatures(observations,actions)
    q_val = sess.run(q.value,feed_dict={q.feature:features})


    for chunk in qn.chunks(range(len(memory)),batch_size):
        featureChunk = features[chunk,:]
        targetChunk = targets[chunk,:]

        mses.append(sess.run(q.mse,feed_dict={q.feature:featureChunk,
        q.target:targetChunk}))
        sess.run(q.train_value,feed_dict={q.feature:featureChunk,
        q.target:targetChunk})


import matplotlib.pyplot as plt
plt.plot(mses)
plt.figure()
plt.plot(q_val[:,0])

#%%
env = gym.make('MountainCar-v0')

for i_episode in range(500):
    obs = env.reset()
    for t in range(int(1e10)):
        env.render()
        #print(obs)
        #qn.printEvery(1000,t)

        # get action
        seed = np.random.rand()
        if seed > 0.2:
            feature = w.getFeatures(repmat(obs,3,1),[0,1,2])
            action_q = sess.run(q.value,feed_dict={q.feature:feature})
            action = np.argmax(action_q)
        else:
            action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps"\
            .format(i_episode, t+1))
            break
#%%
saver.save(sess,baseDir+'models/moutainCar-pretrain-rbf-q-2')
