import numpy as np
import pickle
import rl
import pa
import Q2 as q

from sklearn.pipeline import Pipeline, FeatureUnion
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDRegressor
from sklearn.kernel_approximation import RBFSampler

baseDir = pa.getBaseDir()

feature_size = 100

def getTargets(rewards,gamma=1):
    return np.array(rl.getMC(rewards,gamma))


with open(baseDir+'data/train-mini.pkl','rb') as mf:
    train = pickle.load(mf)

pipe = Pipeline([
    ('scale', StandardScaler()),
    ('rbf1', RBFSampler(gamma=1.0, n_components=100)),
])

fitData = []
actions = []
for memory in train[:20]:
    for item in memory:
        fitData.append(item[0])
        actions.append(item[1])

pipe.fit(fitData)

def getFeatures(observations,actions):
    observations = np.array(observations)
    output = np.zeros([observations.shape[0],feature_size*3])
    features = pipe.transform(observations)
    for i in range(observations.shape[0]):
        output[i,feature_size*actions[i]:feature_size*(actions[i]+1)] = \
        features[i]
    return output

#cc = getFeatures(fitData[:30],actions[:30])
