import tensorflow as tf

q_lr = 0.01
featureLen = 300

with tf.name_scope('input'):
    feature = tf.placeholder(tf.float32, [None,featureLen], 'observation')
    target = tf.placeholder(tf.float32, [None,1], 'target')

with tf.name_scope('q_net'):

    w_q2 = tf.Variable(tf.truncated_normal([featureLen,1], stddev=0.1),
    name='w_q2')
    b_q2 = tf.Variable(tf.constant(0.1), name='b_q2')
    value = tf.matmul(feature, w_q2) + b_q2
    mse = tf.contrib.losses.mean_squared_error(value, target)
    train_value = tf.train.AdamOptimizer(learning_rate=q_lr)\
    .minimize(mse)
