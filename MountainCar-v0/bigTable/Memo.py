import numpy as np
from scipy.spatial.distance import cdist
import pickle
from random import shuffle

#IDEA remove existing entries by timesteps. Those with large timesteps are
#removed first

class Memo():
    def __init__(self,episodes='data.pkl',limit=3000, n=5,
    span=10, keep_size=300, factor=1, use_shuffle=True,forget_by_steps=True):
        self.limit = limit
        self.n = n
        self.span = span
        self.keep_size = keep_size
        self.factor = factor
        self.forget_by_steps = forget_by_steps

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        if use_shuffle:
            shuffle(episodes)

        for i, episode in enumerate(episodes):
            episode = episode[-self.keep_size:]
            for item in episode:
                item.append(800)
            if i==0:
                episode = self.select(episode)
                obs, action, step = self.get(episode)
                self.observations = obs
                self.actions = action
                self.steps = step
            else:
                self.add(episode)
            if self.observations.shape[0] == self.limit:
                break

    def select(self,episode):
        start = np.random.randint(1,self.span+1)
        return [episode[i] for i in range(-start,-len(episode)-1,-self.span)]

    def get(self,episode):
        obs, action, _, step = zip(*episode)
        obs = np.vstack(obs)
        action = np.hstack(action)
        step = np.hstack(step)
        return obs, action, step

    def add(self,episode):
        episode = self.select(episode)
        obs, action, step = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.hstack(( \
            action,
            self.actions
        ))
        self.steps = np.hstack(( \
            step,
            self.steps
        ))
        if self.observations.shape[0] > self.limit:
            if self.forget_by_steps:
                # remove episode of large steps
                idx = np.argsort(self.steps)[:self.limit]
            else:
                idx = range(self.limit)
            self.observations = self.observations[idx,:]
            self.actions = self.actions[idx]
            self.steps = self.steps[idx]

    def sample(self,size):
        idx = np.random.choice(self.observations.shape[0],
        size=size, replace=False)
        return self.observations[idx,:]

    def getActions(self,obs):
        # maybe scale velocity? since velocity is 10 times smaller than position
        # or use cosine distance? Looks like euclidean is better than cosine
        obs = np.array(obs)
        factoredObs = obs.copy()
        factoredObs[:,1] = obs[:,1]*self.factor
        factoredSelfObs = self.observations.copy()
        factoredSelfObs[:,1] = self.observations[:,1]*self.factor

        mat = cdist(factoredObs, factoredSelfObs, 'euclidean')
        actionMat = self.actions[np.argsort(mat)]
        #print(actionMat)
        actionMat = actionMat[:,:self.n]
        # IDEA maybe change to probablility measurement?
        actions = np.apply_along_axis(
            lambda x: np.random.choice(x,size=1)[0],
            axis=1,
            arr=actionMat
        )
        # actions = np.apply_along_axis(
        #         lambda x: np.bincount(x, minlength=3),
        #         axis=1,
        #         arr=actionMat
        #     ).argmax(axis=1)
        return actions

    def getActionsE(self,episode):
        obs, _ = self.get(episode)
        return self.getActions(obs)
