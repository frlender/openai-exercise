import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os

#from model import model
from Memo import Memo

keep_size = 110
#batch_size = 20

memo = Memo(limit=6000,n=5,factor=8,span=1,keep_size=keep_size) # works!

#memo = Memo(limit=6000,n=5,factor=10,span=1,keep_size=keep_size)

## remove cut memory by steps? Doesn't work
#memo = Memo(limit=6000,n=5,factor=8,span=1,keep_size=keep_size,
#            forget_by_steps=False) # does not work

env = gym.make('MountainCar-v0')
#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)
tracker = pa.Tracker()

for i_episode in range(int(2000)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
        action = memo.getActions([obs])[0]
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}."\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)

#    if len(episode) == 200:
#        continue

    for item in episode:
        item.append(t)

    if len(episode) > keep_size:
        episode = episode[-keep_size:]
    memo.add(episode)


#with open('memo.pkl','wb') as mf:
#    pickle.dump(memo,mf)
    # episode = episode[-keep_size:]
    # targets = memo.getActions(episode)
    # episode = zip(episode, targets)
    #
    # for chunk in qn.chunks(episode,batch_size):
    #     obs,
