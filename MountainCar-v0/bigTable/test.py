import numpy as np
from Memo import Memo
import pickle


with open('data.pkl','rb') as df:
    data = pickle.load(df)

memo = Memo()
obs = memo.observations
actions = memo.actions

sObs = memo.sample(3)
sActions = memo.getActions(sObs)

actionMat = np.array([
    [0,0,1,1,1,2],
    [1,0,1,2,2,2],
    [0,1,1,0,0,0],
    [1,1,1,1,1,1]
])


actions = np.apply_along_axis(
        lambda x: np.bincount(x, minlength=3),
        axis=1,
        arr=actionMat
    ).argmax(axis=1)


with open('memo.pkl','rb') as mf:
    data = pickle.load(mf)