import pickle

with open('../../data/train-mini.pkl','rb') as df:
    data = pickle.load(df)

with open('data.pkl','wb') as df:
    data = [[x[:-1] for x in E] for E in data[:100]]
    pickle.dump(data,df)
