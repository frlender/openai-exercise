# pa is abbreviation for openai
import rl
import numpy as np
import qn
import yaml
import gym
from multiprocessing import Pool
from copy import deepcopy

baseDirName = 'exercise-private'

#[estimator.tree_.max_depth for estimator in forest.estimators_]

def getBaseDir():
    return qn.getBaseDir(baseDirName)

def getConfig():
    with open('config.yml','r') as cf:
        return yaml.load(cf)


class SpaceEncoder():
    def __init__(self,env,segCount,overlap=1/10):
        # currently only support observation of box class
        # and action of discrete class
        # segCount: an array of segment count for each dimension of observation
        spans = np.stack([env.observation_space.low,
        env.observation_space.high]).T
        self.action_n = env.action_space.n
        self.obsEncoders = []
        for i,span in enumerate(spans):
            self.obsEncoders.append(
            rl.Discretizer(list(span),segCount[i],overlap))
        self.actionEncoder = rl.OneHotEncoder(list(range(
        env.action_space.n)))

    def _getObsBinaries(self,obs):
        obs = np.array(obs)
        binaries = []
        for i, encoder in enumerate(self.obsEncoders):
            binaries.append(encoder.encode(obs[:,i]))
        return binaries

    def encodeObs(self,obs,ravel=True):
        binaries = self._getObsBinaries(obs)
        #print(binaries)
        return rl.combineBinaryFeatures(binaries,ravel)

    def encodeObsSeperate(self,obs):
        binaries = self._getObsBinaries(obs)
        #print(binaries)
        return binaries

    def encodeAction(self,action):
        return self.actionEncoder.encode(action)

    def encodeObsAction(self,obs,action, ravel=True):
        binaries = self._getObsBinaries(obs)
        actionBinary = self.encodeAction(action)
        return rl.combineBinaryFeatures(binaries+[actionBinary],ravel)

    def encodeObsActionSeperate(self,obs,action):
        binaries = self._getObsBinaries(obs)
        actionBinary = self.encodeAction(action)
        return binaries+[actionBinary]

    def encodeObsActionAdjacent(self,obs,action):
        binaries = self._getObsBinaries(obs)
        obsBinary = np.hstack(binaries)
        obsLen, featureLen = obsBinary.shape
        res = np.zeros([obsLen,featureLen*self.action_n])
        for i, item in enumerate(action):
            start = item*featureLen
            res[i,start:(start+featureLen)] = obsBinary[i,:]
        return res


class AsymetricSpaceEncoder(SpaceEncoder):
    def __init__(self,env,segCounts):
        self.obsEncoders = []
        for segCount in segCounts:
            self.obsEncoders.append(
            rl.AsymetricDiscretizer(segCount))
        self.actionEncoder = rl.OneHotEncoder(list(range(
        env.action_space.n)))

class Tracker():
    def __init__(self,avgBy=100, greater=False):
        # greater determines if the objective is to get more time steps
        # (as in CartPole) or less time steps (as in Mountain Car)
        self.store = []
        self.avgBy = avgBy
        self.greater = greater
        if self.greater:
            self.best = -np.inf
            self.compareFun = lambda current, best: current>best
        else:
            self.best = np.inf
            self.compareFun = lambda current, best: current<best

    def getAvg(self):
        return np.mean(self.store)

    def isBest(self,t):
        if len(self.store) < self.avgBy:
            self.store.append(t)
            return False
        else:
            self.store.pop(0)
            self.store.append(t)
            avg = self.getAvg()
            if self.compareFun(avg,self.best):
                self.best = avg
                return True
            else:
                return False

class _RandomActionObj():
    def __init__(self, env):
        self.env = env

    def getAction(self, obs, eps):
        return self.env.action_space.sample()

class Trainer():
    def __init__(self,envName):
        self.envName = envName
        self.env = gym.make(envName)

    @staticmethod
    def _run_once(args):
        envName = args[0]
        actionObj = args[1]
        eps = args[2]
        max_iter = args[3]

        env = gym.make(envName, silence=True)
        if isinstance(actionObj,str) and actionObj == 'random':
            actionObj = _RandomActionObj(env)

        episode = []
        obs = env.reset()
        for t in range(int(max_iter)):
            action = actionObj.getAction(obs, eps)
            # suppose the env will clip action automatically
            newObs, reward, done, info = env.step(action)
            episode.append([obs,action,reward])
            obs = newObs
            if done:
                break
        return episode, t+1

    def parallel(self,actionObj, eps, session_len, cores=6, max_iter=2000,
        every=20):
        params = []
        for i in range(session_len):
            #copiedActionObj = deepcopy(actionObj)
            params.append((self.envName, actionObj, eps, max_iter))

        with Pool(cores) as p:
            res = []
            for i, item in enumerate(
                p.imap_unordered(self._run_once, params)
            ):
                # qn.printEvery(every, i+1, '{}/{}'.format(i+1,session_len))
                res.append(item)
        return list(zip(*res))

    def run_once(self,actionFun,max_iter=1e6,tracker=None,
    keep_size=None,add_step=False,force_range=True,render=False):
        episode = []
        obs = self.env.reset()
        for t in range(int(max_iter)):
            if render:
                self.env.render()
            action = actionFun(obs)
            if force_range:
                forced_action = np.clip(action,
                    self.env.action_space.low,
                    self.env.action_space.high)
                newObs, reward, done, info = self.env.step(forced_action)
            else:
                newObs, reward, done, info = self.env.step(action)
            episode.append([obs,action,reward])
            obs = newObs
            if done:
                break

        if tracker:
            tracker.isBest(t+1)

        if add_step:
            for item in episode:
                item.append(t+1)

        if keep_size:
            episode = episode[-keep_size:]

        return episode, t+1
