from multiprocessing.managers import BaseManager
from multiprocessing import Pool
from sklearn.ensemble import RandomForestRegressor


class MathsClass:
    def __init__(self):
        self.model = RandomForestRegressor(n_estimators=10)
        self.model.fit([[1,2],[3,4],[5,6]],[2,6,7])
    def add(self, x):
        return self.model.predict(x)

class MyManager(BaseManager):
    pass

MyManager.register('Maths', MathsClass)

with MyManager() as manager:
    inst = manager.Maths()
    # print(inst.add(1,2))
    def f(x):
        return x[0].add(x[1])
    with Pool(3) as p:
        print(p.map(f, [ [ inst, [[1,2]] ],
                         [ inst, [[3,4]] ],
                         [ inst, [[5,6]] ]
                        ]))
