import gym
import pickle

keep_size = 500

env = gym.make('CartPole-v0')
episodes = []
for i_episode in range(int(1000)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
        action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps."\
            .format(i_episode, t+1))
            break
    episodes.append(episode[-keep_size:])

episodes = sorted(episodes,key=lambda x:len(x),reverse=True)
with open('data.pkl','wb') as df:
    pickle.dump(episodes,df)
