import numpy as np
from scipy.spatial.distance import cdist
import pickle
from random import shuffle

#IDEA remove existing entries by timesteps. Those with large timesteps are
#removed first

class Memo():
    def __init__(self,episodes='data.pkl',limit=3000, n=5,
    span=2, keep_size=10, use_shuffle=True):
        self.limit = limit
        self.n = n
        self.span = span
        self.keep_size = keep_size

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        if use_shuffle:
            shuffle(episodes)

        for i, episode in enumerate(episodes):
            if len(episode) > keep_size:
                episode = episode[-self.keep_size:]
            if i==0:
                episode = self.select(episode)
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)
            if self.observations.shape[0] == self.limit:
                break

    def select(self,episode):
        start = np.random.randint(1,self.span+1)
        return [episode[i] for i in range(-start,-len(episode)-1,-self.span)]

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.hstack(action)
        return obs, action

    def add(self,episode):
        episode = self.select(episode)
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.hstack(( \
            action,
            self.actions
        ))
        if self.observations.shape[0] > self.limit:
            # remove episode of large steps
            self.observations = self.observations[:self.limit,:]
            self.actions = self.actions[:self.limit]

    def sample(self,size):
        idx = np.random.choice(self.observations.shape[0],
        size=size, replace=False)
        return self.observations[idx,:]

    def getActions(self,obs):
        mat = cdist(obs, self.observations, 'euclidean')
        actionMat = self.actions[np.argsort(mat)]
        #print(actionMat)
        actionMat = actionMat[:,:self.n]
        # IDEA maybe change to probablility measurement?
        actionMat = np.logical_not(actionMat).astype(int)
        actions = np.apply_along_axis(
            lambda x: np.random.choice(x,size=1)[0],
            axis=1,
            arr=actionMat
        )
        # actions = np.apply_along_axis(
        #         lambda x: np.bincount(x, minlength=3),
        #         axis=1,
        #         arr=actionMat
        #     ).argmax(axis=1)
        return actions

    def getActionsE(self,episode):
        obs, _ = self.get(episode)
        return self.getActions(obs)
