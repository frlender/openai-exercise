import gym
from gym import wrappers
import qn
import numpy as np
import pa
import os
import shutil


#from model import model
from Memo import Memo

keep_size = 7
random_size = 500
#batch_size = 20
#thres = 0.2

#memo = Memo(limit=3000, n=10, span=1, keep_size=keep_size)

env = gym.make('CartPole-v0')
logDir = '../../tmp/CartPole-v0-bigTable'
if os.path.isdir(logDir):
  shutil.rmtree(logDir)
env = wrappers.Monitor(env, logDir)
tracker = pa.Tracker(greater=True)

for i_episode in range(int(2000)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
        if i_episode <= random_size:
            action = env.action_space.sample()
        else:
            action = memo.getActions([obs])[0]
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}."\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)



    if i_episode == 0:
        memo = Memo(episodes=[episode],limit=3000, n=10,
                     span=1, keep_size=keep_size)
    else:
        if len(episode) < 200 or i_episode < random_size:
            if len(episode) > keep_size:
                episode = episode[-keep_size:]
            memo.add(episode)


# with open('memo.pkl','wb') as mf:
#     pickle.dump(memo,mf)
    # episode = episode[-keep_size:]
    # targets = memo.getActions(episode)
    # episode = zip(episode, targets)
    #
    # for chunk in qn.chunks(episode,batch_size):
    #     obs,
