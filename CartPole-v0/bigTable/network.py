import tensorflow as tf

value_lr = 0.1
target_lr = 0.1

with tf.name_scope('input'):
    observation = tf.placeholder(tf.float32, [None,4], 'observation')
    target = tf.placeholder(tf.float32, [None,1], 'target')
    with tf.name_scope('action_net'):
        action = tf.placeholder(tf.float32, [None,2], 'action')

with tf.name_scope('value_net'):
    weights_v1 = tf.Variable(tf.truncated_normal([4,1], stddev=0.5),
    name='weights_v1')
    biases_v1 = tf.Variable(tf.constant(0.1), name='biases_v1')
    value = tf.matmul(observation, weights_v1) + biases_v1

    mse = tf.contrib.losses.mean_squared_error(value, target)
    train_value = tf.train.AdamOptimizer(learning_rate=value_lr)\
    .minimize(mse)

with tf.name_scope('action_net'):
    weights_a1 = tf.Variable(tf.truncated_normal([4,1], stddev=0.1),
    name='weights_a1')
    #biases_a1 = tf.Variable(0.1 , name='biases_a1')
    # None x 1 = None x 4 x 4 x 1
    preactivation_a1 = tf.matmul(observation, weights_a1) # + biases_a1
    # probability for action == 0
    action_prob = tf.nn.softmax(preactivation_a1)

    with tf.name_scope('gradient'):
        # None x 1
