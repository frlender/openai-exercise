from keras.models import Sequential
from keras.layers import Dense, Activation

model = Sequential([
    Dense(2,input_dim=4),
    Activation('relu'),
    Dense(2),
    Activation('softmax')
])
