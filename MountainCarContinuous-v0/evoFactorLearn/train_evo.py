import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle

from Evo import Evo

keep_size = 110
top = 60

# env = gym.make('MountainCarContinuous-v0')
#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)

#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)

tracker = pa.Tracker()
trainer = pa.Trainer('MountainCarContinuous-v0')
evo = Evo(top=top,keep_size=keep_size)

stats = []
episodes = []
for i in range(int(12000)):
    factor = evo.getFactor()
    steps, t = trainer.run_once(
        lambda obs:[evo.getAction(obs,factor)],
        tracker,
        keep_size = keep_size
    )
    print("{}th episode, {} timesteps, best {}, factor {}."\
    .format(i, t, tracker.best, factor))
    episodes.append({'steps':steps, 'factor':factor})

    if (i+1)%600 == 0:
        evo = Evo(episodes,top=top,keep_size=keep_size)
        print('report factors {}, {}'.format(np.mean(evo.factors),
              np.std(evo.factors)))
        stats.append([np.mean(evo.factors),
              np.std(evo.factors)])
        episodes = []
