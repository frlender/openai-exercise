import numpy as np
from scipy.spatial.distance import cdist
import pickle

class Evo():
    def __init__(self,episodes='data.pkl',top=5,n=5,keep_size=110):
        self.n = n

        factorsNone = False
        if isinstance(episodes, str):
            factorsNone = True
            with open(episodes,'rb') as ef:
                episodes_steps = pickle.load(ef)
                episodes = []
                for episode_steps in episodes_steps:
                    episodes.append({'steps':episode_steps})

        topEpisodes = sorted(episodes,key=lambda x:len(x['steps']),
        reverse=False)[:top]
        for i, episode in enumerate(topEpisodes):
            steps = episode['steps'][-keep_size:]
            if i == 0:
                obs, action = self.get(steps)
                self.observations = obs
                self.actions = action
            else:
                self.add(steps)


        if factorsNone:
            self.factors = None
        else:
            self.factors = [x['factor'] for x in topEpisodes]

    def getFactor(self):
        # default factor to 1
        if self.factors:
            while True:
                factor = np.random.normal(np.mean(self.factors),
                np.std(self.factors))
                if factor > 0:
                    break
            return factor
        else:
            while True:
                factor = np.random.normal(30,20)
                if factor > 0:
                    break
            return factor

    def get(self,steps):
        obs, action, _ = zip(*steps)
        obs = np.vstack(obs)
        action = np.hstack(action)
        return obs, action

    def add(self,steps):
        obs, action = self.get(steps)
        self.observations = np.vstack((
            obs,
            self.observations
        ))
        self.actions = np.hstack((
            action,
            self.actions
        ))

    def getAction(self, obs, factor):
        obs = np.array([obs])
        factoredObs = obs.copy()
        factoredObs[:,1] = obs[:,1]*factor
        factoredSelfObs = self.observations.copy()
        factoredSelfObs[:,1] = self.observations[:,1]*factor

        mat = cdist(factoredObs, factoredSelfObs, 'euclidean')
        vector = np.squeeze(mat)
        actionVec = self.actions[np.argsort(vector)]
        actionVec = actionVec[:self.n]
        action = np.random.normal(np.mean(actionVec),np.std(actionVec))
        return action
