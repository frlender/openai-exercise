import numpy as np
from scipy.spatial.distance import cdist
import pickle
from sklearn.preprocessing import StandardScaler

class Evo():
    def __init__(self,episodes='data.pkl',top=5,n=5,keep_size=110):
        self.n = n

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        episodes = sorted(episodes,key=lambda x:len(x),reverse=False)[:top]
        for i, episode in enumerate(episodes):
            episode = episode[-keep_size:]
            if i == 0:
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)

        self.scaler = StandardScaler()
        self.observations = self.scaler.fit_transform(self.observations)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.hstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.hstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs):
        obs = self.scaler.transform([obs])
        mat = cdist(obs, self.observations, 'euclidean')
        vector = np.squeeze(mat)
        actionVec = self.actions[np.argsort(vector)]
        actionVec = actionVec[:self.n]
        mean = np.mean(actionVec)
        std = np.std(actionVec)
        action = np.random.normal(mean,std)
        return action
