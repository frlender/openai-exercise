import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os

#from model import model
from Memo import Memo

keep_size = 110

memo = Memo(limit=6000,n=5,factor=8,span=1,keep_size=keep_size) # works!

env = gym.make('MountainCarContinuous-v0')
logDir = '../tmp/MountainCarContinuous-v0-bigTable'
if os.path.isdir(logDir):
  shutil.rmtree(logDir)
env = wrappers.Monitor(env, logDir)
tracker = pa.Tracker()

for i_episode in range(int(2000)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
        action = memo.getActions([obs])
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps, best {}."\
            .format(i_episode, t+1, tracker.best))
            break

    tracker.isBest(t+1)

    for item in episode:
        item.append(t)

    if len(episode) > keep_size:
        episode = episode[-keep_size:]
    memo.add(episode)
