import unittest
from Memo import Memo

class TestAll(unittest.TestCase):

    def test_Memo(self):
        episodes = [
            [
                [[1,2],0.1,-1],
                [[1,3],0.2,-1]
                [[1,4],0.3,-1],
                [[1,5],0.4,-1]
            ],
            [
                [[2,1],0.5,-1],
                [[2,2],0.6,-1],
                [[2,1],0.7,-1],
                [[2,2],0.8,-1]
            ]
        ]
        memo = Memo(episodes,n=3,span=2,keep_size=4,shuffle=False)


    def test_isupper(self):
        self.assertTrue('FOO'.isupper())
        self.assertFalse('Foo'.isupper())

    def test_split(self):
        s = 'hello world'
        self.assertEqual(s.split(), ['hello', 'world'])
        # check that s.split fails when the separator is not a string
        with self.assertRaises(TypeError):
            s.split(2)

if __name__ == '__main__':
    unittest.main()
