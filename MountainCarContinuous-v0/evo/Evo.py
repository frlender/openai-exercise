import numpy as np
from scipy.spatial.distance import cdist
import pickle

class Evo():
    def __init__(self,episodes='data.pkl',top=5,n=5,
    factor=8,keep_size=110):
        self.n = n
        self.factor = factor

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        episodes = sorted(episodes,key=lambda x:len(x),reverse=False)[:top]
        for i, episode in enumerate(episodes):
            episode = episode[-keep_size:]
            if i == 0:
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.hstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.hstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs):
        obs = np.array([obs])
        factoredObs = obs.copy()
        factoredObs[:,1] = obs[:,1]*self.factor
        factoredSelfObs = self.observations.copy()
        factoredSelfObs[:,1] = self.observations[:,1]*self.factor

        mat = cdist(factoredObs, factoredSelfObs, 'euclidean')
        vector = np.squeeze(mat)
        actionVec = self.actions[np.argsort(vector)]
        actionVec = actionVec[:self.n]
        action = np.random.normal(np.mean(actionVec),np.std(actionVec))
        return action
