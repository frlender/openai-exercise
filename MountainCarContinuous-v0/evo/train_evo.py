import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle

from EvoStandardized import Evo

keep_size = 110
top = 60

# env = gym.make('MountainCarContinuous-v0')
#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)

#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)

tracker = pa.Tracker()
trainer = pa.Trainer('MountainCarContinuous-v0')
evo = Evo(top=top,keep_size=keep_size)

episodes = []
for i in range(int(3000)):
    episode, t = trainer.run_once(
        lambda obs:[evo.getAction(obs)],
        tracker = tracker,
        keep_size = keep_size,
        force_range = True
    )
    print("{}th episode finished after {} timesteps, best {}."\
    .format(i, t, tracker.best))
    episodes.append(episode)

    if (i+1)%200 == 0:
        evo = Evo(episodes,top=top,keep_size=keep_size)
        episodes = []
