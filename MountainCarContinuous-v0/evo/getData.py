import gym
import pickle

keep_size = None

env = gym.make('MountainCarContinuous-v0')
episodes = []
for i_episode in range(int(100)):
    episode = []
    obs = env.reset()
    for t in range(int(1e10)):
        action = env.action_space.sample()
        newObs, reward, done, info = env.step(action)
        episode.append([obs,action,reward])
        obs = newObs
        if done:
            print("{}th episode finished after {} timesteps."\
            .format(i_episode, t+1))
            break
    if keep_size:
        episode = episode[-keep_size:]
    episodes.append(episode)

with open('data.pkl','wb') as df:
   pickle.dump(episodes,df)
