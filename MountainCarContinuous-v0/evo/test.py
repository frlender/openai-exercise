#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Mar 21 23:24:40 2017

@author: qn
"""

from Memo import Memo
episodes = [
            [
                [[1,2],0.1,-1],
                [[1,3],0.2,-1],
                [[1,4],0.3,-1],
                [[1,5],0.4,-1]
            ],
            [
                [[2,1],0.5,-1],
                [[2,2],0.6,-1],
                [[2,3],0.7,-1],
                [[2,4],0.8,-1]
            ]
        ]
memo = Memo(episodes,n=3,span=2,keep_size=4,use_shuffle=False)
print(memo.observations)
print(memo.actions)

episode = [
            [[1,3.5],0.1,-1],
            [[2,2.5],0.1,-1]
        ]
actions = memo.getActionsE(episode)
print(actions)


import pickle
with open('data.pkl','rb') as df:
    episodes = pickle.load(df)

memo = Memo(episodes)
obs = memo.sample(20)
actions = memo.getActions(obs)

import numpy as np
def force_action_in_range(action,force_range):
    lower_range = np.repeat(force_range[0],action.shape)
    lower_stack = np.vstack([lower_range,action])
    selected = np.max(lower_stack,axis=0)
    higher_range = np.repeat(force_range[1],action.shape)
    higher_stack = np.vstack([selected,higher_range])
    forced_action = np.min(higher_stack,axis=0)
    return forced_action