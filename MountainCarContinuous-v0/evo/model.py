from keras.models import Sequential
from keras.layers import Dense, Activation

model = Sequential([
    Dense(120,input_dim=2),
    Activation('relu'),
    Dense(1),
    Activation('tanh')
])

model.compile(loss='mean_squared_error',optimizer='adam')
