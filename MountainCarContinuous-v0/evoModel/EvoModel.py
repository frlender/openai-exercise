import pickle
from sklearn.preprocessing import PolynomialFeatures
from sklearn.linear_model import LinearRegression
from sklearn.pipeline import Pipeline
import numpy as np
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor



class Evo():
    def __init__(self,episodes='data.pkl',top=0.1, keep_size=110):

        if isinstance(episodes, str):
            with open(episodes,'rb') as ef:
                episodes = pickle.load(ef)

        topSize = int(len(episodes)*top)
        episodes = sorted(episodes,key=lambda x:len(x),
        reverse=False)[:topSize]

        print('added {} episodes.'.format(len(episodes)))

        for i, episode in enumerate(episodes):
            episode = episode[-keep_size:]
            if i == 0:
                obs, action = self.get(episode)
                self.observations = obs
                self.actions = action
            else:
                self.add(episode)

        self.model = Pipeline([
            ('rf', RandomForestRegressor())
        ])
        self.model.fit(self.observations,self.actions.ravel())
        self.actionStd = np.std(self.actions, axis=0)

    def getMaxReturn(self,episode):
        cumsum = np.cumsum([x[2] for x in episode])
        idx = np.argmax(cumsum)
        return idx, cumsum[idx]

    def get(self,episode):
        obs, action, _ = zip(*episode)
        obs = np.vstack(obs)
        action = np.vstack(action)
        return obs, action

    def add(self,episode):
        obs, action = self.get(episode)
        self.observations = np.vstack(( \
            obs,
            self.observations
        ))
        self.actions = np.vstack(( \
            action,
            self.actions
        ))

    def getAction(self,obs, epsilon):
        actionMean = self.model.predict([obs])
        mark = np.random.rand()
        if mark > epsilon:
            return actionMean
        else:
            action = np.random.normal(actionMean,self.actionStd)
            return action

    # def sample(self):
    #     # sample an observation
    #     idx = np.random.choice(self.observations.shape[0],
    #     size=size, replace=False)
    #     return self.observations[idx,:]

    # def getStd(self,n=5):
    #     stds = []
    #     for i in range(n):
    #         obs = self.sample()
    #         mat = cdist([obs], self.observations, 'euclidean')
    #         vector = np.squeeze(mat)
    #         actionMat = self.actions[np.argsort(vector),:]
    #         actionMat = actionMat[:n,:]
    #         std = np.std(actionMat,axis=0)
    #         stds.append(std)
    #     return np.mean(np.array(stds),axis=0)

    # def getAction(self,obs):
    #     obs = self.scaler.transform([obs])
    #     mat = cdist(obs, self.observations, 'euclidean')
        # vector = np.squeeze(mat)
        # actionMat = self.actions[np.argsort(vector),:]
        # actionMat = actionMat[:self.n,:]
        # means = np.mean(actionMat,axis=0)
        # stds = np.std(actionMat,axis=0)
    #     action = np.random.normal(means,stds)
    #     return action

        # obs = self.scaler.transform([obs])
        # mat = cdist(obs, self.observations, 'euclidean')
        # vector = np.squeeze(mat)
        # actionVec = self.actions[np.argsort(vector)]
        # actionVec = actionVec[:self.n]
        # action = np.random.normal(np.mean(actionVec),np.std(actionVec))
        # return action
