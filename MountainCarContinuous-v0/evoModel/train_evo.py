import gym
from gym import wrappers
import qn
import numpy as np
import pa
import pickle
import shutil
import os
import pickle
import matplotlib.pyplot as plt

from EvoModel import Evo

# top = 60
# session_len = 300

top = 0.1
session_len = 100
epsilon = lambda x:np.abs(x-80)/240

# env = gym.make('MountainCarContinuous-v0')
#logDir = '../../tmp/MountainCar-v0-bigTable'
#if os.path.isdir(logDir):
#  shutil.rmtree(logDir)
#env = wrappers.Monitor(env, logDir)

#with open('data.pkl','rb') as ef:
#    episodes = pickle.load(ef)

tracker = pa.Tracker(greater=False)
trainer = pa.Trainer('MountainCarContinuous-v0')
evo = Evo(episodes='../evo/data.pkl',top=top)

episodes = []
step_counts = []
performance = []

for i in range(3000):
    episode, t = trainer.run_once(
            lambda obs:evo.getAction(obs,epsilon(tracker.best)),
            max_iter=5000
    )
    tracker.isBest(t)
    print("{}th episode, {} timesteps, best {:.3f}."\
    .format(i, t, tracker.best))
    episodes.append(episode)

    if (i+1)%10 == 0:
        performance.append([i+1, tracker.best])

    if (i+1)%session_len == 0:
        evo = Evo(episodes,top=top)
        episodes = []
