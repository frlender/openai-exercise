import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt

import gym

import pa
import network as q
baseDir = pa.getBaseDir()

saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver.restore(sess, baseDir+
'solutions/policy-gradient/model/moutainCar-policy-gradient-pretrain-simple-ahead')

count = 50
env = gym.make('MountainCar-v0')
spans = np.stack([env.observation_space.low,
        env.observation_space.high]).T
grid = np.meshgrid(np.linspace(spans[0,0],spans[0,1], count),
                   np.linspace(spans[1,0],spans[1,1], count))

obs = np.stack((np.ravel(grid[0]),np.ravel(grid[1]))).T
action_prob = sess.run(q.action_prob,feed_dict={q.observation:obs})
action = np.argmax(action_prob,axis=1)
plt.scatter(obs[:,0], obs[:,1], c=action, cmap='viridis')