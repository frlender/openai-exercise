import tensorflow as tf
import numpy as np
import matplotlib.pyplot as plt
import sys

import gym

import pa
baseDir = pa.getBaseDir()
sys.path.append(baseDir+'solutions/Q-learning/')

import Q as q
from util import QLearn


saver = tf.train.Saver()
sess = tf.InteractiveSession()
sess.run(tf.global_variables_initializer())
saver.restore(sess, baseDir+
'solutions/Q-learning/model/moutainCar-train-q-Q-learning-360')
ql = QLearn(sess,q)

count = 50
env = gym.make('MountainCar-v0')
spans = np.stack([env.observation_space.low,
        env.observation_space.high]).T
grid = np.meshgrid(np.linspace(spans[0,0],spans[0,1], count),
                   np.linspace(spans[1,0],spans[1,1], count))

obs = np.stack((np.ravel(grid[0]),np.ravel(grid[1]))).T
action_q = ql.getQ(obs)
action = np.argmax(action_q,axis=1)
max_q = np.amax(action_q,axis=1)

plt.scatter(obs[:,0], obs[:,1], c=action, cmap='jet')

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
ax.scatter(obs[:,0], obs[:,1],max_q)

